# Storage service

You can store file in this service. Each file must be linked to a "section" which is just a group for files.

Stack:

* Spring Boot 2.4.5
* Java 11+
* PostgreSQL 11+


## How to install

You can use a standalone install (JAR file) or use the official Docker image.

You can use the `storage-schema.sql` dump file to initialize your database.

### Standalone install

You have to download the last JAR file in the [repository](https://gitlab.com/sirius6/storage-service/-/packages).

Then you have to configure the `application.yml` and store it in the same directory (see [configuration](#configuration)).

### Docker image

The official Docker image is `fabar/sirius-storage`. It is stored on [Docker Hub](https://hub.docker.com/r/fabar/sirius-storage).

The default exposed port is 8080. It also exposes the volume `/app/var` which will contains the stored file tree.

### Configuration

The configuration file `application.yml` should be place in the same directory of the JAR file.

```yaml
spring:
  datasource:
    password: sirius
    url: jdbc:postgresql://db:5432/storage
    username: sirius
  jpa:
    hibernate:
      ddl-auto: update
    show-sql: false

storage:
  directory: /app/var
  masterPassword: sirius
```

Here are the parameters to use:

| Key                                | Type    | Default value | Description                                |
|------------------------------------|---------|---------------|--------------------------------------------|
| storage.directory                  | String  | var           | Path to the directory storing the files    |
| storage.masterPassword             | String  |               | The password used to cipher the sections   |
| storage.transactionTimeout         | Integer | 15            | The transaction expiration (in minutes)    |
| credentials.login                  | String  | sirius        | The API login                              |
| credentials.password               | String  | sirius        | The API password                           |
| credentials.web.asyncMaxPoolSize   | Integer | 10            | The max thread count for async requests    |
| credentials.web.asyncQueueCapacity | Integer | 30            | The max capacity of async requests         |
| credentials.web.asyncTimeout       | Integer | 5             | The async request timeout (in seconds)     |
| spring.datasource.username         | String  |               | The database username                      |
| spring.datasource.password         | String  |               | The database password                      |
| spring.datasource.url              | String  |               | The database URL                           |


## API

You can use the swagger-ui by following these steps:

* launch the application. You can start it from the source with `./gradlew dockerComposeUp`
* go to [http://localhost:8080/swagger-ui.html](http://localhost:8080/swagger-ui.html)

The default credentials are `sirius:sirius`.

## Transactional storage

You can use the transactional system to store files. For this, just create the transaction by using the `/transaction` API.
After, you have to add the `transactionUuid` parameter when creating files and section.
