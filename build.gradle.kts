import io.sirius.docker.DockerRestart
import io.sirius.docker.DockerExec
import io.sirius.dsl.appVersion
import io.sirius.dsl.configureJacoco
import org.jetbrains.kotlin.gradle.tasks.KotlinCompile
import org.springframework.boot.gradle.tasks.run.BootRun

plugins {
	id("org.springframework.boot") version "2.4.5"
	id("io.spring.dependency-management") version "1.0.11.RELEASE"
	id("org.sonarqube") version "3.0"
	id("jacoco")
	id("maven-publish")
	kotlin("jvm") version "1.4.31"
	kotlin("plugin.spring") version "1.4.31"
	kotlin("plugin.jpa") version "1.4.31"
	id("sirius") version "2.2.1"
}

java.sourceCompatibility = JavaVersion.VERSION_11
java.targetCompatibility = JavaVersion.VERSION_11

configureJacoco()

tasks.getByName<BootRun>("bootRun") {
	systemProperty("spring.profiles.active", "dev")
}

tasks.findByName("dockerComposeUp")?.dependsOn("assemble")

dockerCompose {
	environment = mapOf("APP_VERSION" to project.appVersion)
}

tasks.register<DockerRestart>("updateService") {
	group = "docker"
	dependsOn("assemble")
	service = "server"
}

tasks.register<DockerExec>("dumpSchema") {
	group = "postgres"
	doLast {
		dockerExec("storage-db") {
			commands = listOf("pg_dump", "-U", "sirius", "-s", "storage")
			output = outputFile(File(projectDir,"storage-schema.sql"))
		}
	}
}

dependencies {
	implementation("org.springframework.boot:spring-boot-starter-batch")
	implementation("org.springframework.boot:spring-boot-starter-data-jpa")
	implementation("org.springframework.boot:spring-boot-starter-security")
	implementation("org.springframework.boot:spring-boot-starter-web")
	implementation("org.springframework.boot:spring-boot-starter-validation")
	implementation("com.fasterxml.jackson.module:jackson-module-kotlin")
	implementation("org.jetbrains.kotlin:kotlin-reflect")
	implementation("org.jetbrains.kotlin:kotlin-stdlib-jdk8")
	implementation("org.liquibase:liquibase-core")
	implementation("org.apache.tika:tika-core:1.26")
	implementation("org.springdoc:springdoc-openapi-ui:1.3.1")

	developmentOnly("org.springframework.boot:spring-boot-devtools")

	runtimeOnly("org.postgresql:postgresql")

	testImplementation("io.mockk:mockk:1.10.0")
	testImplementation("com.h2database:h2")
	testImplementation("com.google.jimfs:jimfs:1.2")
	testImplementation("org.springframework.boot:spring-boot-starter-test")
	testImplementation("org.springframework.batch:spring-batch-test")
	testImplementation("org.springframework.security:spring-security-test")
}

tasks.withType<KotlinCompile> {
	kotlinOptions {
		freeCompilerArgs = listOf("-Xjsr305=strict")
		jvmTarget = "11"
	}
}

tasks.withType<Test> {
	useJUnitPlatform()
}
