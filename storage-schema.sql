--
-- PostgreSQL database dump
--

-- Dumped from database version 11.8 (Debian 11.8-1.pgdg90+1)
-- Dumped by pg_dump version 11.8 (Debian 11.8-1.pgdg90+1)

SET statement_timeout = 0;
SET lock_timeout = 0;
SET idle_in_transaction_session_timeout = 0;
SET client_encoding = 'UTF8';
SET standard_conforming_strings = on;
SELECT pg_catalog.set_config('search_path', '', false);
SET check_function_bodies = false;
SET xmloption = content;
SET client_min_messages = warning;
SET row_security = off;

SET default_tablespace = '';

SET default_with_oids = false;

--
-- Name: batch_job_execution; Type: TABLE; Schema: public; Owner: sirius
--

CREATE TABLE public.batch_job_execution (
    job_execution_id bigint NOT NULL,
    version bigint,
    job_instance_id bigint NOT NULL,
    create_time timestamp without time zone NOT NULL,
    start_time timestamp without time zone,
    end_time timestamp without time zone,
    status character varying(10),
    exit_code character varying(2500),
    exit_message character varying(2500),
    last_updated timestamp without time zone,
    job_configuration_location character varying(2500)
);


ALTER TABLE public.batch_job_execution OWNER TO sirius;

--
-- Name: batch_job_execution_context; Type: TABLE; Schema: public; Owner: sirius
--

CREATE TABLE public.batch_job_execution_context (
    job_execution_id bigint NOT NULL,
    short_context character varying(2500) NOT NULL,
    serialized_context text
);


ALTER TABLE public.batch_job_execution_context OWNER TO sirius;

--
-- Name: batch_job_execution_params; Type: TABLE; Schema: public; Owner: sirius
--

CREATE TABLE public.batch_job_execution_params (
    job_execution_id bigint NOT NULL,
    type_cd character varying(6) NOT NULL,
    key_name character varying(100) NOT NULL,
    string_val character varying(250),
    date_val timestamp without time zone,
    long_val bigint,
    double_val double precision,
    identifying character(1) NOT NULL
);


ALTER TABLE public.batch_job_execution_params OWNER TO sirius;

--
-- Name: batch_job_execution_seq; Type: SEQUENCE; Schema: public; Owner: sirius
--

CREATE SEQUENCE public.batch_job_execution_seq
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


ALTER TABLE public.batch_job_execution_seq OWNER TO sirius;

--
-- Name: batch_job_instance; Type: TABLE; Schema: public; Owner: sirius
--

CREATE TABLE public.batch_job_instance (
    job_instance_id bigint NOT NULL,
    version bigint,
    job_name character varying(100) NOT NULL,
    job_key character varying(32) NOT NULL
);


ALTER TABLE public.batch_job_instance OWNER TO sirius;

--
-- Name: batch_job_seq; Type: SEQUENCE; Schema: public; Owner: sirius
--

CREATE SEQUENCE public.batch_job_seq
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


ALTER TABLE public.batch_job_seq OWNER TO sirius;

--
-- Name: batch_step_execution; Type: TABLE; Schema: public; Owner: sirius
--

CREATE TABLE public.batch_step_execution (
    step_execution_id bigint NOT NULL,
    version bigint NOT NULL,
    step_name character varying(100) NOT NULL,
    job_execution_id bigint NOT NULL,
    start_time timestamp without time zone NOT NULL,
    end_time timestamp without time zone,
    status character varying(10),
    commit_count bigint,
    read_count bigint,
    filter_count bigint,
    write_count bigint,
    read_skip_count bigint,
    write_skip_count bigint,
    process_skip_count bigint,
    rollback_count bigint,
    exit_code character varying(2500),
    exit_message character varying(2500),
    last_updated timestamp without time zone
);


ALTER TABLE public.batch_step_execution OWNER TO sirius;

--
-- Name: batch_step_execution_context; Type: TABLE; Schema: public; Owner: sirius
--

CREATE TABLE public.batch_step_execution_context (
    step_execution_id bigint NOT NULL,
    short_context character varying(2500) NOT NULL,
    serialized_context text
);


ALTER TABLE public.batch_step_execution_context OWNER TO sirius;

--
-- Name: batch_step_execution_seq; Type: SEQUENCE; Schema: public; Owner: sirius
--

CREATE SEQUENCE public.batch_step_execution_seq
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


ALTER TABLE public.batch_step_execution_seq OWNER TO sirius;

--
-- Name: section; Type: TABLE; Schema: public; Owner: sirius
--

CREATE TABLE public.section (
    uuid uuid NOT NULL,
    cipher_key character varying(128),
    codename character varying(128) NOT NULL,
    creation_date timestamp without time zone NOT NULL,
    expiration_date timestamp without time zone,
    transaction_uuid uuid
);


ALTER TABLE public.section OWNER TO sirius;

--
-- Name: storage_transaction; Type: TABLE; Schema: public; Owner: sirius
--

CREATE TABLE public.storage_transaction (
    uuid uuid NOT NULL,
    creation_date timestamp without time zone NOT NULL,
    expiration_date timestamp without time zone
);


ALTER TABLE public.storage_transaction OWNER TO sirius;

--
-- Name: stored_file; Type: TABLE; Schema: public; Owner: sirius
--

CREATE TABLE public.stored_file (
    uuid uuid NOT NULL,
    content_type character varying(64) NOT NULL,
    creation_date timestamp without time zone NOT NULL,
    expiration_date timestamp without time zone,
    file_size bigint NOT NULL,
    filename character varying(100) NOT NULL,
    last_download_date timestamp without time zone,
    last_modification_date timestamp without time zone,
    sha256 character varying(64) NOT NULL,
    section_uuid uuid NOT NULL,
    transaction_uuid uuid
);


ALTER TABLE public.stored_file OWNER TO sirius;

--
-- Name: stored_file_metadata; Type: TABLE; Schema: public; Owner: sirius
--

CREATE TABLE public.stored_file_metadata (
    uuid uuid NOT NULL,
    name character varying(64) NOT NULL,
    value character varying(256) NOT NULL,
    stored_file_uuid uuid NOT NULL
);


ALTER TABLE public.stored_file_metadata OWNER TO sirius;

--
-- Name: batch_job_execution_context batch_job_execution_context_pkey; Type: CONSTRAINT; Schema: public; Owner: sirius
--

ALTER TABLE ONLY public.batch_job_execution_context
    ADD CONSTRAINT batch_job_execution_context_pkey PRIMARY KEY (job_execution_id);


--
-- Name: batch_job_execution batch_job_execution_pkey; Type: CONSTRAINT; Schema: public; Owner: sirius
--

ALTER TABLE ONLY public.batch_job_execution
    ADD CONSTRAINT batch_job_execution_pkey PRIMARY KEY (job_execution_id);


--
-- Name: batch_job_instance batch_job_instance_pkey; Type: CONSTRAINT; Schema: public; Owner: sirius
--

ALTER TABLE ONLY public.batch_job_instance
    ADD CONSTRAINT batch_job_instance_pkey PRIMARY KEY (job_instance_id);


--
-- Name: batch_step_execution_context batch_step_execution_context_pkey; Type: CONSTRAINT; Schema: public; Owner: sirius
--

ALTER TABLE ONLY public.batch_step_execution_context
    ADD CONSTRAINT batch_step_execution_context_pkey PRIMARY KEY (step_execution_id);


--
-- Name: batch_step_execution batch_step_execution_pkey; Type: CONSTRAINT; Schema: public; Owner: sirius
--

ALTER TABLE ONLY public.batch_step_execution
    ADD CONSTRAINT batch_step_execution_pkey PRIMARY KEY (step_execution_id);


--
-- Name: batch_job_instance job_inst_un; Type: CONSTRAINT; Schema: public; Owner: sirius
--

ALTER TABLE ONLY public.batch_job_instance
    ADD CONSTRAINT job_inst_un UNIQUE (job_name, job_key);


--
-- Name: stored_file_metadata metadata_idx; Type: CONSTRAINT; Schema: public; Owner: sirius
--

ALTER TABLE ONLY public.stored_file_metadata
    ADD CONSTRAINT metadata_idx UNIQUE (name, stored_file_uuid);


--
-- Name: section section_codename_idx; Type: CONSTRAINT; Schema: public; Owner: sirius
--

ALTER TABLE ONLY public.section
    ADD CONSTRAINT section_codename_idx UNIQUE (codename);


--
-- Name: section section_pkey; Type: CONSTRAINT; Schema: public; Owner: sirius
--

ALTER TABLE ONLY public.section
    ADD CONSTRAINT section_pkey PRIMARY KEY (uuid);


--
-- Name: storage_transaction storage_transaction_pkey; Type: CONSTRAINT; Schema: public; Owner: sirius
--

ALTER TABLE ONLY public.storage_transaction
    ADD CONSTRAINT storage_transaction_pkey PRIMARY KEY (uuid);


--
-- Name: stored_file_metadata stored_file_metadata_pkey; Type: CONSTRAINT; Schema: public; Owner: sirius
--

ALTER TABLE ONLY public.stored_file_metadata
    ADD CONSTRAINT stored_file_metadata_pkey PRIMARY KEY (uuid);


--
-- Name: stored_file stored_file_name_idx; Type: CONSTRAINT; Schema: public; Owner: sirius
--

ALTER TABLE ONLY public.stored_file
    ADD CONSTRAINT stored_file_name_idx UNIQUE (filename, section_uuid);


--
-- Name: stored_file stored_file_pkey; Type: CONSTRAINT; Schema: public; Owner: sirius
--

ALTER TABLE ONLY public.stored_file
    ADD CONSTRAINT stored_file_pkey PRIMARY KEY (uuid);


--
-- Name: stored_file_metadata fk92c5lrv8t2r46qs3ce5ofuo7n; Type: FK CONSTRAINT; Schema: public; Owner: sirius
--

ALTER TABLE ONLY public.stored_file_metadata
    ADD CONSTRAINT fk92c5lrv8t2r46qs3ce5ofuo7n FOREIGN KEY (stored_file_uuid) REFERENCES public.stored_file(uuid) ON DELETE CASCADE;


--
-- Name: stored_file fknhye0awb0dosua0nhs2obvx8q; Type: FK CONSTRAINT; Schema: public; Owner: sirius
--

ALTER TABLE ONLY public.stored_file
    ADD CONSTRAINT fknhye0awb0dosua0nhs2obvx8q FOREIGN KEY (section_uuid) REFERENCES public.section(uuid) ON DELETE CASCADE;


--
-- Name: stored_file fkq5q6mnpfflpklxwldymcfcrxa; Type: FK CONSTRAINT; Schema: public; Owner: sirius
--

ALTER TABLE ONLY public.stored_file
    ADD CONSTRAINT fkq5q6mnpfflpklxwldymcfcrxa FOREIGN KEY (transaction_uuid) REFERENCES public.storage_transaction(uuid);


--
-- Name: section fkqgvg0pjipdrwlgipw3tc4tmq7; Type: FK CONSTRAINT; Schema: public; Owner: sirius
--

ALTER TABLE ONLY public.section
    ADD CONSTRAINT fkqgvg0pjipdrwlgipw3tc4tmq7 FOREIGN KEY (transaction_uuid) REFERENCES public.storage_transaction(uuid) ON DELETE CASCADE;


--
-- Name: batch_job_execution_context job_exec_ctx_fk; Type: FK CONSTRAINT; Schema: public; Owner: sirius
--

ALTER TABLE ONLY public.batch_job_execution_context
    ADD CONSTRAINT job_exec_ctx_fk FOREIGN KEY (job_execution_id) REFERENCES public.batch_job_execution(job_execution_id);


--
-- Name: batch_job_execution_params job_exec_params_fk; Type: FK CONSTRAINT; Schema: public; Owner: sirius
--

ALTER TABLE ONLY public.batch_job_execution_params
    ADD CONSTRAINT job_exec_params_fk FOREIGN KEY (job_execution_id) REFERENCES public.batch_job_execution(job_execution_id);


--
-- Name: batch_step_execution job_exec_step_fk; Type: FK CONSTRAINT; Schema: public; Owner: sirius
--

ALTER TABLE ONLY public.batch_step_execution
    ADD CONSTRAINT job_exec_step_fk FOREIGN KEY (job_execution_id) REFERENCES public.batch_job_execution(job_execution_id);


--
-- Name: batch_job_execution job_inst_exec_fk; Type: FK CONSTRAINT; Schema: public; Owner: sirius
--

ALTER TABLE ONLY public.batch_job_execution
    ADD CONSTRAINT job_inst_exec_fk FOREIGN KEY (job_instance_id) REFERENCES public.batch_job_instance(job_instance_id);


--
-- Name: batch_step_execution_context step_exec_ctx_fk; Type: FK CONSTRAINT; Schema: public; Owner: sirius
--

ALTER TABLE ONLY public.batch_step_execution_context
    ADD CONSTRAINT step_exec_ctx_fk FOREIGN KEY (step_execution_id) REFERENCES public.batch_step_execution(step_execution_id);


--
-- PostgreSQL database dump complete
--

