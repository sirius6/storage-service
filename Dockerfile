FROM openjdk:11-jre-slim

# Set time zone
ENV TZ=Europe/Paris
RUN ln -snf /usr/share/zoneinfo/$TZ /etc/localtime && echo $TZ > /etc/timezone

# Start server
COPY build/libs/storage-service-*.jar /app/storage-service.jar
COPY docker/application.yml /app/application.yml
WORKDIR "/app"
ENTRYPOINT ["java", "-jar", "/app/storage-service.jar", "-Xmx256m"]

VOLUME "/app/var"
EXPOSE 8080
