package io.sirius.storage

sealed class StorageException(msg: String, th: Throwable? = null) : Exception(msg, th)

class UnknownStoredFileException(msg: String) : StorageException(msg)

class UnknownSectionException(msg: String) : StorageException(msg)

class UnknownTransactionException(msg: String) : StorageException(msg)

class FileNotReadableException(msg: String) : StorageException(msg)

class FileNotWritableException(msg: String) : StorageException(msg)

class InvalidFileHashException(msg: String) : StorageException(msg)

class SectionAlreadyExistsException(msg: String) : StorageException(msg)