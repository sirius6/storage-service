package io.sirius.storage.dto

import io.swagger.v3.oas.annotations.media.Schema
import java.time.Instant
import java.util.*
import javax.validation.constraints.NotBlank
import javax.validation.constraints.NotNull
import javax.validation.constraints.Pattern

data class SectionCreateRequest(
    @field:NotBlank
    @field:Pattern(regexp = "^[a-z0-9-_]+$", flags = [Pattern.Flag.CASE_INSENSITIVE])
    @field:Schema(description = "The section unique codename", required = true)
    val codename: String,

    @field:NotNull
    @field:Schema(description = "Indicates if the storage is ciphered", defaultValue = "true", required = false)
    val secure: Boolean = true,

    @field:Schema(description = "The section expiration date (ISO8601)", required = false, example = "2021-04-22T06:00:00Z")
    val expirationDate: Instant? = null,

    @field:Schema(description = "The transaction identifier (only when using transactional section)")
    val transactionUuid: UUID? = null
)