package io.sirius.storage.dto

import io.swagger.v3.oas.annotations.media.Schema
import java.util.*
import javax.validation.constraints.NotEmpty
import javax.validation.constraints.NotNull

data class MetadataUpdateRequest(
    @field:NotNull
    @field:Schema(description = "The metadata to apply", required = true)
    val metadata: Map<String, String> = emptyMap(),

    @field:Schema(
        description = "The transaction identifier to use (only for transactional files)",
        example = "26920ab8-a443-11eb-8e63-7f1350819f3c",
        required = false
    )
    val transactionUuid: UUID? = null,
)