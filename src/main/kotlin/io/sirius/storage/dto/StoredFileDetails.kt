package io.sirius.storage.dto

import io.swagger.v3.oas.annotations.media.Schema
import java.net.URI
import java.time.Instant
import java.util.*

data class StoredFileDetails(
    @field:Schema(description = "The file unique identifier", example = "68fec2f8-a446-11eb-953d-efe2781a3efa")
    val uuid: UUID,

    @field:Schema(description = "The file creation date", example = "2021-04-22T06:00:00Z")
    val creationDate: Instant,

    @field:Schema(description = "The last file download date", example = "2021-04-22T06:00:00Z")
    val lastDownloadDate: Instant?,

    @field:Schema(description = "The file last modification date", example = "2021-04-22T06:00:00Z")
    val lastModificationDate: Instant?,

    @field:Schema(description = "The file expiration date", example = "2021-04-22T06:00:00Z")
    val expirationDate: Instant?,

    @field:Schema(description = "The file size in bytes", example = "128000")
    val fileSize: Long,

    @field:Schema(description = "The file name", example = "image.png")
    val filename: String,

    @field:Schema(description = "The file content type", example = "image/png")
    val contentType: String,

    @field:Schema(description = "The file SHA256 hash in hexa", example = "53ff2fcb614f9549ed99858386b92f1cddbe39e163567edfb7b0199c4083be09")
    val sha256: String,

    @field:Schema(description = "The file metadata")
    val metadata: Map<String, String> = emptyMap(),

    @field:Schema(description = "The file direct download URI", example = "/direct/my-section/file/image.png")
    val directUri: URI
)