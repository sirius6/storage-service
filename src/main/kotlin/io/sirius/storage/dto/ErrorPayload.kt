package io.sirius.storage.dto

data class ErrorPayload(
    val message: String,
    val invalidFields: Set<String>? = null
)