package io.sirius.storage.dto

import org.springframework.web.multipart.MultipartFile
import java.time.Instant
import java.util.*

data class StoredFileCreateRequest(
    val file: MultipartFile,
    val transactionUuid: UUID? = null,
    val sha256: String? = null,
    val expirationDate: Instant? = null
)