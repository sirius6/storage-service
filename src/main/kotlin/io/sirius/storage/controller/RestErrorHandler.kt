package io.sirius.storage.controller

import io.sirius.storage.SectionAlreadyExistsException
import io.sirius.storage.StorageException
import io.sirius.storage.dto.ErrorPayload
import org.springframework.http.HttpHeaders
import org.springframework.http.HttpStatus
import org.springframework.http.MediaType
import org.springframework.http.ResponseEntity
import org.springframework.validation.FieldError
import org.springframework.web.bind.MethodArgumentNotValidException
import org.springframework.web.bind.annotation.ExceptionHandler
import org.springframework.web.bind.annotation.RestControllerAdvice
import org.springframework.web.context.request.WebRequest
import org.springframework.web.servlet.mvc.method.annotation.ResponseEntityExceptionHandler

@RestControllerAdvice
class RestErrorHandler : ResponseEntityExceptionHandler() {

    @ExceptionHandler(StorageException::class)
    fun handleStorageException(
        ex: StorageException,
        request: WebRequest
    ): ResponseEntity<Any> = buildResponse(
        ex,
        HttpStatus.BAD_REQUEST,
        ErrorPayload(ex.message ?: "Server error")
    )

    @ExceptionHandler(SectionAlreadyExistsException::class)
    fun handleSectionAlreadyExistsException(
        ex: SectionAlreadyExistsException,
        request: WebRequest
    ): ResponseEntity<Any> = buildResponse(
        ex,
        HttpStatus.CONFLICT,
        ErrorPayload(ex.message ?: "Server error")
    )

    override fun handleMethodArgumentNotValid(
        ex: MethodArgumentNotValidException,
        headers: HttpHeaders,
        status: HttpStatus,
        request: WebRequest
    ): ResponseEntity<Any> {
        val invalidFields = ex.bindingResult.allErrors.map { error ->
            require(error is FieldError)
            error.field
        }.toSet()

        return buildResponse(ex, status, ErrorPayload("Invalid parameters", invalidFields))
    }

    override fun handleExceptionInternal(
        ex: Exception,
        body: Any?,
        headers: HttpHeaders,
        status: HttpStatus,
        request: WebRequest
    ): ResponseEntity<Any> {
        val message: String = when {
            status.is5xxServerError -> "Server error"
            else -> ex.message ?: "Server error"
        }
        return buildResponse(ex, status, ErrorPayload(message))
    }

    private fun buildResponse(
        ex: Exception,
        status: HttpStatus,
        payload: ErrorPayload
    ): ResponseEntity<Any> {
        when {
            status.is5xxServerError -> logger.error("Server error (status=${status.value()})", ex)
            else -> logger.warn("Server warning (status=${status.value()}, msg=${ex.message})")
        }
        return ResponseEntity.status(status)
            .contentType(MediaType.APPLICATION_JSON)
            .body(payload)
    }

}