package io.sirius.storage.controller

import io.sirius.storage.controller.StoredFileController.Companion.PARAM_SECTION_UUID
import io.sirius.storage.dto.MetadataUpdateRequest
import io.sirius.storage.dto.StoredFileCreateRequest
import io.sirius.storage.dto.StoredFileDetails
import io.sirius.storage.io.StoredFileResource
import io.sirius.storage.service.StoredFileService
import io.sirius.storage.util.toDetails
import io.sirius.storage.util.toUUID
import io.swagger.v3.oas.annotations.Operation
import io.swagger.v3.oas.annotations.media.Schema
import io.swagger.v3.oas.annotations.tags.Tag
import org.springframework.core.io.Resource
import org.springframework.http.MediaType
import org.springframework.http.ResponseEntity
import org.springframework.web.bind.annotation.*
import org.springframework.web.multipart.MultipartFile
import java.time.Instant
import java.util.*
import javax.validation.Valid

@RestController
@RequestMapping("/api/section/{$PARAM_SECTION_UUID}/file")
@Tag(name = "File API")
class StoredFileController(private val storedFileService: StoredFileService) {

    companion object {
        internal const val PARAM_SECTION_UUID = "sectionUuid"
        private const val PARAM_TRANSACTION_UUID = "transactionUuid"
        private const val PARAM_EXPIRATION_DATE = "expirationDate"
        private const val PARAM_FILE_UUID = "fileUuid"
        private const val PARAM_FILE = "file"
        private const val PARAM_HASH = "sha256"
    }

    @PostMapping(consumes = [MediaType.MULTIPART_FORM_DATA_VALUE])
    @Operation(summary = "Add a file to a section")
    fun addFile(
        @PathVariable(PARAM_SECTION_UUID) sectionUuid: UUID,

        @Schema(description = "The file to store", required = true)
        @RequestPart(PARAM_FILE) file: MultipartFile,

        @Schema(description = "The file SHA-256 hash in hexa", required = false)
        @RequestPart(PARAM_HASH, required = false) sha256: String?,

        @Schema(description = "The transaction identifier (only for transactional file)", required = false)
        @RequestPart(PARAM_TRANSACTION_UUID, required = false) transactionUuid: String?,

        @Schema(description = "The file expiration date", required = false, example = "2021-04-22T06:00:00Z")
        @RequestParam(PARAM_EXPIRATION_DATE, required = false) expirationDate: Instant?
    ): StoredFileDetails = storedFileService.storeFile(
        sectionUuid,
        StoredFileCreateRequest(file, transactionUuid?.toUUID(), sha256, expirationDate)
    ).toDetails()

    @GetMapping
    @Operation(summary = "Get a file from a section")
    fun getSectionStoredFiles(
        @PathVariable(PARAM_SECTION_UUID) sectionUuid: UUID
    ): List<StoredFileDetails> = storedFileService.getSectionStoredFiles(sectionUuid).map { it.toDetails() }

    @PutMapping("{$PARAM_FILE_UUID}", consumes = [MediaType.MULTIPART_FORM_DATA_VALUE])
    @Operation(summary = "Update a file in a section")
    fun updateFile(
        @PathVariable(PARAM_SECTION_UUID) sectionUuid: UUID,

        @Schema(description = "The file identifier to update")
        @PathVariable(PARAM_FILE_UUID) fileUuid: UUID,

        @Schema(description = "The file to update", required = true)
        @RequestPart(PARAM_FILE) file: MultipartFile,

        @Schema(description = "The file SHA-256 hash in hexa", required = false)
        @RequestPart(PARAM_HASH, required = false) sha256: String?,

        @Schema(description = "The transaction identifier (only for transactional file)", required = false)
        @RequestPart(PARAM_TRANSACTION_UUID, required = false) transactionUuid: String?,
    ): StoredFileDetails = storedFileService.updateFile(
        sectionUuid,
        fileUuid,
        StoredFileCreateRequest(file, transactionUuid?.toUUID(), sha256)
    ).toDetails()

    @GetMapping("{$PARAM_FILE_UUID}")
    @Operation(summary = "Get a file details from a section")
    fun getStoredFile(
        @PathVariable(PARAM_SECTION_UUID) sectionUuid: UUID,

        @Schema(description = "The file identifier to get")
        @PathVariable(PARAM_FILE_UUID) fileUuid: UUID,

        @Schema(description = "The transaction identifier (only for transactional file)", required = false)
        @RequestParam(PARAM_TRANSACTION_UUID, required = false) transactionUuid: UUID?
    ): StoredFileDetails = storedFileService.getStoredFile(sectionUuid, fileUuid, transactionUuid).toDetails()

    @DeleteMapping("{$PARAM_FILE_UUID}")
    @Operation(summary = "Delete a file from a section")
    fun deleteFile(
        @PathVariable(PARAM_SECTION_UUID) sectionUuid: UUID,

        @Schema(description = "The file identifier to delete")
        @PathVariable(PARAM_FILE_UUID) fileUuid: UUID,

        @Schema(description = "The transaction identifier (only for transactional file)", required = false)
        @RequestParam(PARAM_TRANSACTION_UUID, required = false) transactionUuid: UUID?
    ): Unit = storedFileService.deleteFile(sectionUuid, fileUuid, transactionUuid)

    @GetMapping("{$PARAM_FILE_UUID}/download")
    @Operation(summary = "Download a file from a section")
    fun downloadFile(
        @PathVariable(PARAM_SECTION_UUID) sectionUuid: UUID,

        @Schema(description = "The file identifier to download")
        @PathVariable(PARAM_FILE_UUID) fileUuid: UUID,

        @Schema(description = "The transaction identifier (only for transactional file)", required = false)
        @RequestParam(PARAM_TRANSACTION_UUID, required = false) transactionUuid: UUID?
    ): ResponseEntity<Resource> {
        val resource: StoredFileResource = storedFileService.getFileResource(sectionUuid, fileUuid, transactionUuid)
        return ResponseEntity.ok()
            .contentType(resource.mediaType)
            .body(resource)
    }

    @PutMapping("{$PARAM_FILE_UUID}/metadata")
    @Operation(summary = "Change file metadata")
    fun updateFileMetadata(
        @PathVariable(PARAM_SECTION_UUID) sectionUuid: UUID,
        @PathVariable(PARAM_FILE_UUID) fileUuid: UUID,
        @RequestBody @Valid request: MetadataUpdateRequest
    ): Unit = storedFileService.updateMetadata(sectionUuid, fileUuid, request)
}