package io.sirius.storage.controller

import io.sirius.storage.io.StoredFileResource
import io.sirius.storage.service.StoredFileService
import io.swagger.v3.oas.annotations.Operation
import io.swagger.v3.oas.annotations.security.SecurityRequirement
import io.swagger.v3.oas.annotations.tags.Tag
import org.springframework.core.io.Resource
import org.springframework.http.ContentDisposition
import org.springframework.http.HttpHeaders
import org.springframework.http.ResponseEntity
import org.springframework.web.bind.annotation.*

@RestController
@RequestMapping("/direct")
@Tag(name = "Direct file API")
class DirectFileController(private val storedFileService: StoredFileService) {

    companion object {
        private const val PARAM_SECTION_CODENAME = "sectionCodename"
        private const val PARAM_FILENAME = "filename"
        private const val PARAM_DISPOSITION = "disposition"
    }

    @GetMapping("{$PARAM_SECTION_CODENAME}/{$PARAM_FILENAME}")
    @Operation(summary = "Download a file by its section codename and file name (no UUID)")
    fun downloadFile(
        @PathVariable(PARAM_SECTION_CODENAME) sectionCodename: String,
        @PathVariable(PARAM_FILENAME) filename: String,
        @RequestParam(PARAM_DISPOSITION, required = false) disposition: String?
    ): ResponseEntity<Resource> {
        val resource: StoredFileResource = storedFileService.getFileResource(sectionCodename, filename)
        val dispositionHeader = ContentDisposition.builder(disposition ?: "attachment")
            .filename(filename)
            .build()
        return ResponseEntity.ok()
            .contentType(resource.mediaType)
            .also { resource.storedFile.metadata.forEach { md -> it.header("x-sirius-${md.name}", md.value) } }
            .header(HttpHeaders.CONTENT_DISPOSITION, dispositionHeader.toString())
            .body(resource)
    }
}