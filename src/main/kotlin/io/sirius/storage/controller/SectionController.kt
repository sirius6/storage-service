package io.sirius.storage.controller

import io.sirius.storage.dto.SectionCreateRequest
import io.sirius.storage.model.Section
import io.sirius.storage.service.SectionService
import io.swagger.v3.oas.annotations.Operation
import io.swagger.v3.oas.annotations.tags.Tag
import org.springframework.http.ContentDisposition
import org.springframework.http.HttpHeaders
import org.springframework.http.MediaType
import org.springframework.http.ResponseEntity
import org.springframework.web.bind.annotation.*
import org.springframework.web.servlet.mvc.method.annotation.StreamingResponseBody
import java.util.*
import javax.validation.Valid

@RestController
@RequestMapping("/api/section")
@Tag(name = "Section API")
class SectionController(private val sectionService: SectionService) {

    companion object {
        private const val PARAM_SECTION_UUID = "sectionUuid"
        private const val PARAM_TRANSACTION_UUID = "transactionUuid"
        private val MEDIA_TYPE_ZIP = MediaType.parseMediaType("application/zip")
    }

    @PostMapping
    @Operation(summary = "Create a new section")
    fun createSection(@RequestBody @Valid request: SectionCreateRequest): Section =
        sectionService.createSection(request)

    @GetMapping("{$PARAM_SECTION_UUID}")
    @Operation(summary = "Get a section by its identifier")
    fun getSection(
        @PathVariable(PARAM_SECTION_UUID) sectionUuid: UUID,
        @RequestParam(PARAM_TRANSACTION_UUID, required = false) transactionUuid: UUID?
    ): Section = sectionService.getSection(sectionUuid, transactionUuid)

    @DeleteMapping("{$PARAM_SECTION_UUID}")
    @Operation(summary = "Delete a new section")
    fun deleteSection(
        @PathVariable(PARAM_SECTION_UUID) sectionUuid: UUID,
        @RequestParam(PARAM_TRANSACTION_UUID, required = false) transactionUuid: UUID?
    ): Unit = sectionService.deleteSection(sectionUuid, transactionUuid)

    @GetMapping("{$PARAM_SECTION_UUID}/archive")
    @Operation(summary = "Create a ZIP archive with all section files")
    fun getSectionArchive(
        @PathVariable(PARAM_SECTION_UUID) sectionUuid: UUID,
        @RequestParam(PARAM_TRANSACTION_UUID, required = false) transactionUuid: UUID?
    ): ResponseEntity<StreamingResponseBody> {
        val archive = sectionService.getSectionArchive(sectionUuid, transactionUuid)
        val dispositionHeader = ContentDisposition.attachment()
            .filename(archive.filename)
            .build()
        return ResponseEntity.ok()
            .header(HttpHeaders.CONTENT_DISPOSITION, dispositionHeader.toString())
            .contentType(MEDIA_TYPE_ZIP)
            .body(archive)
    }
}