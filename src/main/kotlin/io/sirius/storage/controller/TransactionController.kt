package io.sirius.storage.controller

import io.sirius.storage.dto.StoredFileDetails
import io.sirius.storage.model.Section
import io.sirius.storage.model.StorageTransaction
import io.sirius.storage.service.StorageTransactionService
import io.sirius.storage.util.toDetails
import io.swagger.v3.oas.annotations.Operation
import io.swagger.v3.oas.annotations.tags.Tag
import org.springframework.web.bind.annotation.*
import java.util.*

@RestController
@RequestMapping("/api/transaction")
@Tag(name = "Transaction API")
class TransactionController(
    private val transactionService: StorageTransactionService
) {

    companion object {
        private const val PARAM_TRANSACTION_UUID = "transactionUuid"
    }

    @PostMapping
    @Operation(summary = "Create a new transaction")
    fun createTransaction(): StorageTransaction = transactionService.createTransaction()

    @GetMapping("{$PARAM_TRANSACTION_UUID}/file")
    @Operation(summary = "Get the files in the transaction")
    fun getTransactionFiles(
        @PathVariable(PARAM_TRANSACTION_UUID) transactionUuid: UUID
    ): List<StoredFileDetails> = transactionService.getStoredFiles(transactionUuid).map { it.toDetails() }

    @GetMapping("{$PARAM_TRANSACTION_UUID}/section")
    @Operation(summary = "Get the sections in the transaction")
    fun getTransactionSections(
        @PathVariable(PARAM_TRANSACTION_UUID) transactionUuid: UUID
    ): List<Section> = transactionService.getSections(transactionUuid)

    @PostMapping("{$PARAM_TRANSACTION_UUID}/commit")
    @Operation(summary = "Commit the transaction", description = "The files saved in this transaction will be available for everyone")
    fun commitTransaction(
        @PathVariable(PARAM_TRANSACTION_UUID) transactionUuid: UUID
    ): Unit = transactionService.commitTransaction(transactionUuid)

    @PostMapping("{$PARAM_TRANSACTION_UUID}/rollback")
    @Operation(summary = "Rollback the transaction", description = "The files saved in this transaction will be deleted")
    fun rollbackTransaction(
        @PathVariable(PARAM_TRANSACTION_UUID) transactionUuid: UUID
    ): Unit = transactionService.rollbackTransaction(transactionUuid)
}