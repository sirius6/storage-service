package io.sirius.storage.batch

import io.sirius.storage.SystemFaultException
import io.sirius.storage.util.logger
import org.springframework.batch.core.ExitStatus
import org.springframework.batch.core.Job
import org.springframework.batch.core.JobParametersBuilder
import org.springframework.batch.core.launch.JobLauncher
import org.springframework.scheduling.annotation.Scheduled
import org.springframework.stereotype.Component
import java.time.temporal.ChronoUnit

@Component
class BatchRunner(
    private val jobLauncher: JobLauncher,
    private val expirationJob: Job
) {

    private val logger = logger()

    @Scheduled(fixedDelay = 600_000) // 10 min
    fun executeExpirationJob() = expirationJob.execute()


    private fun Job.execute() {
        logger.info("Running the {} job...", name)
        val params = JobParametersBuilder()
            .addString("JobID", System.currentTimeMillis().toString())
            .toJobParameters()

        // Execute job
        val jobExecution = jobLauncher.run(this, params)

        // Check result
        if (!jobExecution.exitStatus.equals(ExitStatus.COMPLETED)) {
            throw SystemFaultException(name + " batch has crashed with reason: " + jobExecution.exitStatus)
        }
        val duration = jobExecution.startTime.toInstant().until(jobExecution.endTime.toInstant(), ChronoUnit.SECONDS)
        val failuresCount = jobExecution.failureExceptions.size
        logger.info("{} job is completed in {} seconds ({} failures)", name, duration, failuresCount)
    }

}