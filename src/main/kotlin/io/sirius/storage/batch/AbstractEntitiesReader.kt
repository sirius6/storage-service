package io.sirius.storage.batch

import io.sirius.storage.model.BaseEntity
import org.springframework.batch.item.ItemReader

abstract class AbstractEntitiesReader<T : BaseEntity> : ItemReader<T> {

    private var entities: Iterator<T>? = null

    override fun read(): T? {
        if (entities == null) {
            entities = findEntities().iterator()
        }
        return if (!entities!!.hasNext()) null
        else entities?.next()
    }

    protected abstract fun findEntities(): List<T>

}