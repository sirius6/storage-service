package io.sirius.storage.batch

import io.sirius.storage.model.StoredFile
import io.sirius.storage.repository.StoredFileRepository
import io.sirius.storage.service.StoredFileService
import io.sirius.storage.util.logger
import io.sirius.storage.util.runMdc
import org.springframework.batch.core.configuration.annotation.StepScope
import org.springframework.batch.item.ItemProcessor
import org.springframework.stereotype.Component
import java.time.Instant

@Component
@StepScope
class StoreFileExpirationReader(
    private val repository: StoredFileRepository
) : AbstractEntitiesReader<StoredFile>() {

    override fun findEntities(): List<StoredFile> =
        repository.findByExpirationDateBefore(Instant.now())

}

@Component
@StepScope
class StoredFileExpirationProcessor(
    private val storedFileService: StoredFileService
) : ItemProcessor<StoredFile, StoredFile> {

    private val logger = logger()

    override fun process(item: StoredFile): StoredFile? {
        item.runMdc {
            logger.info("Will expire stored file {}", item.uuid)
            storedFileService.deleteFile(item)
        }
        return item
    }

}