package io.sirius.storage.batch

import io.sirius.storage.model.StorageTransaction
import io.sirius.storage.repository.StorageTransactionRepository
import io.sirius.storage.service.StorageTransactionService
import org.springframework.batch.core.configuration.annotation.StepScope
import org.springframework.batch.item.ItemProcessor
import org.springframework.stereotype.Component
import java.time.Instant

@Component
@StepScope
class StorageTransactionExpirationReader(
    private val repository: StorageTransactionRepository
) : AbstractEntitiesReader<StorageTransaction>() {

    override fun findEntities(): List<StorageTransaction> =
        repository.findByExpirationDateBefore(Instant.now())

}

@Component
@StepScope
class StorageTransactionExpirationProcessor(
    private val transactionService: StorageTransactionService
) : ItemProcessor<StorageTransaction, StorageTransaction> {

    override fun process(item: StorageTransaction): StorageTransaction? {
        transactionService.rollbackTransaction(item.uuid)
        return item
    }

}