package io.sirius.storage.batch

import io.sirius.storage.model.Section
import io.sirius.storage.repository.SectionRepository
import io.sirius.storage.service.SectionService
import io.sirius.storage.util.runMdc
import org.springframework.batch.core.configuration.annotation.StepScope
import org.springframework.batch.item.ItemProcessor
import org.springframework.stereotype.Component
import java.time.Instant

@Component
@StepScope
class SectionExpirationReader(
    private val repository: SectionRepository
) : AbstractEntitiesReader<Section>() {

    override fun findEntities(): List<Section> =
        repository.findByExpirationDateBefore(Instant.now())

}

@Component
@StepScope
class SectionExpirationProcessor(
    private val sectionService: SectionService
) : ItemProcessor<Section, Section> {

    override fun process(item: Section): Section? {
        item.runMdc {
            // We only delete non-transactional sections
            // Transactional section can expire with its transaction
            sectionService.deleteSection(item.uuid, null)
        }
        return item
    }
}