package io.sirius.storage.batch

import io.sirius.storage.model.BaseEntity
import io.sirius.storage.util.logger
import org.springframework.batch.item.ItemWriter
import org.springframework.data.jpa.repository.JpaRepository
import java.util.*

class DeleteEntitiesWriter<T : BaseEntity>(
    private val repository: JpaRepository<T, UUID>
) : ItemWriter<T> {

    private val logger = logger()

    override fun write(items: List<T>) {
        repository.deleteAll(items)
        logger.debug("Entities deleted: $items")
    }
}