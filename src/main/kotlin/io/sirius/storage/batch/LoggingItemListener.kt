package io.sirius.storage.batch

import io.sirius.storage.util.logger
import org.springframework.batch.core.listener.ItemListenerSupport

class LoggingItemListener<I, O> : ItemListenerSupport<I, O>() {

    private val logger = logger()

    override fun onReadError(ex: Exception) =
        logger.error("Batch read has crashed", ex)

    override fun onProcessError(item: I, ex: Exception) =
        logger.error("Batch process has crashed for item {}", getItemDesc(item), ex)

    override fun onWriteError(ex: Exception, items: List<O>) {
        val desc = items.joinToString { getItemDesc(it) }
        logger.error("Batch write has crashed for item {}", desc, ex)
    }

    private fun getItemDesc(item: Any?) = "${item?.javaClass?.simpleName} $item"

}
