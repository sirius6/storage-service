package io.sirius.storage.config

import io.swagger.v3.oas.models.Components
import io.swagger.v3.oas.models.OpenAPI
import io.swagger.v3.oas.models.info.Info
import io.swagger.v3.oas.models.security.SecurityRequirement
import io.swagger.v3.oas.models.security.SecurityScheme
import org.apache.tika.Tika
import org.apache.tika.config.TikaConfig
import org.springframework.context.annotation.Bean
import org.springframework.context.annotation.Configuration
import org.springframework.context.annotation.Profile
import java.nio.file.FileSystem
import java.nio.file.FileSystems
import java.util.*
import javax.crypto.SecretKey
import javax.crypto.SecretKeyFactory
import javax.crypto.spec.PBEKeySpec
import javax.crypto.spec.SecretKeySpec

@Configuration
class AppConfig(private val properties: StorageProperties) {

    @Bean
    fun fileSystem(): FileSystem = FileSystems.getDefault()

    @Profile("!test")
    @Bean
    fun masterKey(): SecretKey {
        // FIXME: the salt is constant (so bad!)
        val salt = ByteArray(16)
        Arrays.fill(salt, 0x00.toByte())

        val factory = SecretKeyFactory.getInstance("PBKDF2WithHmacSHA256")
        val spec = PBEKeySpec(properties.masterPassword.toCharArray(), salt, 1024, 256)
        val key = factory.generateSecret(spec)
        return SecretKeySpec(key.encoded, "AES")
    }

    @Bean
    fun tika() = Tika()

    @Bean
    fun tikaConfig(): TikaConfig = TikaConfig.getDefaultConfig()

    @Bean
    fun customOpenAPI(): OpenAPI {
        val securitySchemeName = "basicAuth"
        return OpenAPI()
            .info(
                Info()
                    .title("Storage service API")
            )
            .addSecurityItem(SecurityRequirement().addList(securitySchemeName))
            .components(
                Components()
                    .addSecuritySchemes(
                        securitySchemeName,
                        SecurityScheme()
                            .name(securitySchemeName)
                            .type(SecurityScheme.Type.HTTP)
                            .scheme("basic")
                    )
            )

    }
}