package io.sirius.storage.config

import io.sirius.storage.batch.*
import io.sirius.storage.model.Section
import io.sirius.storage.model.StorageTransaction
import io.sirius.storage.model.StoredFile
import io.sirius.storage.repository.SectionRepository
import io.sirius.storage.repository.StorageTransactionRepository
import io.sirius.storage.repository.StoredFileRepository
import org.springframework.batch.core.*
import org.springframework.batch.core.configuration.annotation.EnableBatchProcessing
import org.springframework.batch.core.configuration.annotation.JobBuilderFactory
import org.springframework.batch.core.configuration.annotation.StepBuilderFactory
import org.springframework.batch.core.configuration.annotation.StepScope
import org.springframework.batch.core.launch.support.RunIdIncrementer
import org.springframework.batch.core.step.skip.AlwaysSkipItemSkipPolicy
import org.springframework.batch.core.step.skip.SkipPolicy
import org.springframework.batch.item.ItemProcessor
import org.springframework.batch.item.ItemReader
import org.springframework.batch.item.ItemWriter
import org.springframework.context.annotation.Bean
import org.springframework.context.annotation.Configuration
import org.springframework.scheduling.annotation.EnableScheduling
import org.springframework.scheduling.annotation.SchedulingConfigurer
import org.springframework.scheduling.concurrent.ThreadPoolTaskScheduler
import org.springframework.scheduling.config.ScheduledTaskRegistrar

@Configuration
@EnableBatchProcessing
@EnableScheduling
class BatchConfig(
    private val jobs: JobBuilderFactory,
    private val steps: StepBuilderFactory
) : SchedulingConfigurer {

    override fun configureTasks(taskRegistrar: ScheduledTaskRegistrar) {
        taskRegistrar.setTaskScheduler(batchScheduler())
    }

    @Bean
    fun batchScheduler(): ThreadPoolTaskScheduler {
        val taskScheduler = ThreadPoolTaskScheduler()
        taskScheduler.poolSize = 1
        taskScheduler.setThreadNamePrefix("batch-")
        taskScheduler.initialize()
        return taskScheduler
    }

    @Bean
    fun skipPolicy(): SkipPolicy = AlwaysSkipItemSkipPolicy()

    @Bean
    fun runIdIncrementer() = RunIdIncrementer()

    private fun <I, O> createStep(
        name: String,
        reader: ItemReader<I>,
        processor: ItemProcessor<I, O>,
        writer: ItemWriter<O>
    ): Step {
        val itemListener = LoggingItemListener<I, O>()
        return steps[name]
            .chunk<I, O>(1) // Process one entry at a time
            .reader(reader)
            .processor(processor)
            .writer(writer)
            .faultTolerant()
            .skipPolicy(skipPolicy())
            .listener((itemListener as ItemReadListener<I>))
            .listener((itemListener as ItemProcessListener<I, O>))
            .listener((itemListener as ItemWriteListener<O>))
            .build()
    }


    ////////////////// Expirations //////////////////

    @Bean
    fun sectionExpirationStep(
        reader: SectionExpirationReader,
        processor: SectionExpirationProcessor,
        sectionWriter: ItemWriter<Section>,
    ): Step = createStep("sectionExpirationStep", reader, processor, sectionWriter)

    @Bean
    fun storedFileExpirationStep(
        reader: StoreFileExpirationReader,
        processor: StoredFileExpirationProcessor,
        storedFileWriter: ItemWriter<StoredFile>,
    ): Step = createStep("storedFileExpirationStep", reader, processor, storedFileWriter)

    @Bean
    fun storageTransactionExpirationStep(
        reader: StorageTransactionExpirationReader,
        processor: StorageTransactionExpirationProcessor,
        storageTransactionWriter: ItemWriter<StorageTransaction>,
    ): Step = createStep("storageTransactionExpirationStep", reader, processor, storageTransactionWriter)

    @Bean
    @StepScope
    fun sectionWriter(
        repository: SectionRepository
    ): ItemWriter<Section> = DeleteEntitiesWriter(repository)

    @Bean
    @StepScope
    fun storedFileWriter(
        repository: StoredFileRepository
    ): ItemWriter<StoredFile> = DeleteEntitiesWriter(repository)

    @Bean
    @StepScope
    fun storageTransactionWriter(
        repository: StorageTransactionRepository
    ): ItemWriter<StorageTransaction> = DeleteEntitiesWriter(repository)

    @Bean
    fun expirationJob(
        sectionExpirationStep: Step,
        storageTransactionExpirationStep: Step,
        storedFileExpirationStep: Step,
    ): Job {
        return jobs["expirationJob"]
            .preventRestart()
            .incrementer(runIdIncrementer())
            .start(sectionExpirationStep)
            .next(storageTransactionExpirationStep)
            .next(storedFileExpirationStep)
            .build()
    }

}