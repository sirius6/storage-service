package io.sirius.storage.config

import org.springframework.boot.context.properties.ConfigurationProperties
import org.springframework.stereotype.Component
import java.time.Duration

@Component
@ConfigurationProperties("storage")
class StorageProperties {

    var directory: String = "var"
    var credentials: Credentials = Credentials()
    var web: Web = Web()

    lateinit var masterPassword: String
    var transactionTimeout: Long = 15 // In minutes

    class Credentials {
        var login: String = "sirius"
        var password: String = "sirius"
    }

    class Web {
        var asyncMaxPoolSize: Int = 10
        var asyncQueueCapacity: Int = 30
        var asyncTimeout: Long = Duration.ofMinutes(5).seconds
    }
}