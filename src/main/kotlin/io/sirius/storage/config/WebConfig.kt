package io.sirius.storage.config

import org.springframework.context.annotation.Bean
import org.springframework.context.annotation.Configuration
import org.springframework.core.task.AsyncTaskExecutor
import org.springframework.scheduling.concurrent.ThreadPoolTaskExecutor
import org.springframework.web.servlet.config.annotation.AsyncSupportConfigurer
import org.springframework.web.servlet.config.annotation.WebMvcConfigurer


@Configuration
class WebConfig(private val properties: StorageProperties) : WebMvcConfigurer {

    override fun configureAsyncSupport(configurer: AsyncSupportConfigurer) {
        configurer
            .setDefaultTimeout(properties.web.asyncTimeout)
            .setTaskExecutor(asyncExecutor())
    }

    @Bean
    fun asyncExecutor(): AsyncTaskExecutor {
        val executor = ThreadPoolTaskExecutor()
        executor.maxPoolSize = properties.web.asyncMaxPoolSize
        executor.setQueueCapacity(properties.web.asyncQueueCapacity)
        executor.setThreadNamePrefix("async-http-")
        return executor
    }
}