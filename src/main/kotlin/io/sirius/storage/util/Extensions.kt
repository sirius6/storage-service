package io.sirius.storage.util

import io.sirius.storage.dto.StoredFileDetails
import io.sirius.storage.model.Section
import io.sirius.storage.model.StoredFile
import org.slf4j.Logger
import org.slf4j.LoggerFactory
import org.slf4j.MDC
import org.springframework.core.io.InputStreamSource
import org.springframework.data.repository.CrudRepository
import org.springframework.security.crypto.codec.Hex
import org.springframework.web.util.UriComponentsBuilder
import java.security.MessageDigest
import java.util.*
import javax.crypto.Cipher
import javax.crypto.SecretKey

fun Any.logger(clazz: Class<*> = javaClass): Logger = LoggerFactory.getLogger(clazz)

fun <T> CrudRepository<T, UUID>.findByUuid(uuid: UUID): T? = findById(uuid).orElse(null)

fun InputStreamSource.sha256(): String {
    val digester = MessageDigest.getInstance("SHA-256")

    inputStream.use {
        val buffer = ByteArray(DEFAULT_BUFFER_SIZE)
        var bytes = it.read(buffer)
        while (bytes >= 0) {
            digester.update(buffer, 0, bytes)
            bytes = it.read(buffer)
        }
    }
    val digest = digester.digest()
    return Hex.encode(digest).concatToString().toLowerCase()
}

fun String?.trimToNull(): String? {
    return if (this.isNullOrBlank()) {
        null
    } else {
        this
    }
}

fun String.toUUID(): UUID = UUID.fromString(this)

fun StoredFile.toDetails() = StoredFileDetails(
    uuid = uuid,
    creationDate = creationDate,
    lastDownloadDate = lastDownloadDate,
    lastModificationDate = lastModificationDate,
    fileSize = fileSize,
    filename = filename,
    contentType = contentType,
    sha256 = sha256,
    metadata = metadata.associate { it.name to it.value },
    expirationDate = expirationDate,
    directUri = UriComponentsBuilder
        .fromPath("/direct/{sectionCodename}/{filename}")
        .build(section.codename, filename)
)

fun SecretKey.encrypt(data: ByteArray): ByteArray {
    val cipher = Cipher.getInstance("AES")
    cipher.init(Cipher.ENCRYPT_MODE, this)
    return cipher.doFinal(data)
}

fun SecretKey.decrypt(data: ByteArray): ByteArray {
    val cipher = Cipher.getInstance("AES")
    cipher.init(Cipher.DECRYPT_MODE, this)
    return cipher.doFinal(data)
}

fun StoredFile.runMdc(action: () -> Unit) {
    try {
        mdc()
        action()
    } finally {
        MDC.clear()
    }
}

fun Section.runMdc(action: () -> Unit) {
    try {
        mdc()
        action()
    } finally {
        MDC.clear()
    }
}

fun StoredFile.mdc() {
    section.mdc()

    MDC.put("file.name", filename)
    MDC.put("file.uuid", uuid.toString())
    metadata.forEach { MDC.put("file.metadata.${it.name}", it.value) }
}

fun Section.mdc() {
    MDC.put("section.codename", codename)
    MDC.put("section.uuid", uuid.toString())
}