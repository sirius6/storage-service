package io.sirius.storage.util

import org.springframework.security.core.AuthenticationException
import org.springframework.security.web.AuthenticationEntryPoint
import javax.servlet.http.HttpServletRequest
import javax.servlet.http.HttpServletResponse


class RestAuthenticationEntryPoint : AuthenticationEntryPoint {

    private val logger = logger()

    override fun commence(
            httpServletRequest: HttpServletRequest?,
            httpServletResponse: HttpServletResponse,
            ex: AuthenticationException
    ) {
        logger.error("Responding with unauthorized error. Message - {}", ex.message)
        httpServletResponse.sendError(HttpServletResponse.SC_FORBIDDEN, ex.localizedMessage)
    }

}