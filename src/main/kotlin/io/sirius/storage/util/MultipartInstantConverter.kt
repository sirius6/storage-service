package io.sirius.storage.util

import org.springframework.core.convert.converter.Converter
import org.springframework.stereotype.Component
import org.springframework.web.multipart.MultipartFile
import java.time.Instant
import java.util.*

@Component
class MultipartInstantConverter : Converter<MultipartFile, Instant> {

    override fun convert(source: MultipartFile): Instant? =
        Instant.parse(source.bytes.decodeToString())
}