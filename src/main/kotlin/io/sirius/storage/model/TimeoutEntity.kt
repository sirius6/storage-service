package io.sirius.storage.model

import java.time.Instant

interface TimeoutEntity {

    /** The date the entity will expire */
    val expirationDate: Instant?
}