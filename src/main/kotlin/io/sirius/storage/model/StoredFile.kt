package io.sirius.storage.model

import org.hibernate.annotations.OnDelete
import org.hibernate.annotations.OnDeleteAction
import org.hibernate.annotations.UpdateTimestamp
import java.time.Instant
import java.util.*
import javax.persistence.*

@Entity
@Table(indexes = [Index(name = "stored_file_name_idx", columnList = "filename,section_uuid", unique = true)])
@NamedEntityGraph(name = "StoredFile.WithMetadata", attributeNodes = [NamedAttributeNode("metadata")])
data class StoredFile(
    @Id
    @GeneratedValue
    @Column(nullable = false, length = 36)
    override val uuid: UUID = UUID.randomUUID(),

    @Column(nullable = false)
    val creationDate: Instant = Instant.now(),

    @Column(nullable = true)
    var lastDownloadDate: Instant? = null,

    @Column(nullable = true)
    @UpdateTimestamp
    val lastModificationDate: Instant? = null,

    @Column(nullable = true)
    override val expirationDate: Instant? = null,

    @Column(nullable = false)
    var fileSize: Long,

    @Column(nullable = false, length = 100)
    var filename: String,

    @Column(nullable = false, length = 64)
    var contentType: String,

    @Column(nullable = false, length = 64)
    var sha256: String,

    @OneToMany(mappedBy = "storedFile")
    val metadata: List<StoredFileMetadata> = emptyList(),

    @ManyToOne
    @JoinColumn(nullable = false)
    @OnDelete(action = OnDeleteAction.CASCADE)
    val section: Section,

    @ManyToOne
    @JoinColumn(nullable = true)
    @OnDelete(action = OnDeleteAction.NO_ACTION)
    override var transaction: StorageTransaction? = null

) : BaseEntity, TimeoutEntity, TransactionalEntity