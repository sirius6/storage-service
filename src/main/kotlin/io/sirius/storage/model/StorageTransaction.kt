package io.sirius.storage.model

import io.swagger.v3.oas.annotations.media.Schema
import java.time.Instant
import java.util.*
import javax.persistence.*

@Entity
@Table
data class StorageTransaction(
    @Id
    @GeneratedValue
    @Column(nullable = false, length = 36)
    @field:Schema(description = "The transaction unique identifier")
    override val uuid: UUID = UUID.randomUUID(),

    @Column(nullable = false)
    @field:Schema(description = "The transaction creation date", example = "2021-04-22T06:00:00Z")
    val creationDate: Instant = Instant.now(),

    @Column(nullable = true)
    @field:Schema(description = "The transaction expiration date", example = "2021-04-22T06:00:00Z")
    override val expirationDate: Instant? = null

) : BaseEntity, TimeoutEntity