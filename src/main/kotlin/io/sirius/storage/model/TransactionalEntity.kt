package io.sirius.storage.model

interface TransactionalEntity {

    var transaction: StorageTransaction?
}