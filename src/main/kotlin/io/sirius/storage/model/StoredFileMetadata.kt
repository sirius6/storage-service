package io.sirius.storage.model

import org.hibernate.annotations.OnDelete
import org.hibernate.annotations.OnDeleteAction
import java.util.*
import javax.persistence.*

@Entity
@Table(indexes = [Index(name = "metadata_idx", columnList = "name,stored_file_uuid", unique = true)])
data class StoredFileMetadata(
    @Id
    @GeneratedValue
    @Column(nullable = false, length = 36)
    override val uuid: UUID = UUID.randomUUID(),

    @Column(nullable = false, length = 64)
    val name: String,

    @Column(nullable = false, length = 256)
    val value: String,

    @ManyToOne
    @JoinColumn(nullable = false)
    @OnDelete(action = OnDeleteAction.CASCADE)
    val storedFile: StoredFile
) : BaseEntity