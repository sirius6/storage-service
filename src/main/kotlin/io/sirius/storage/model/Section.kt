package io.sirius.storage.model

import com.fasterxml.jackson.annotation.JsonIgnore
import io.swagger.v3.oas.annotations.media.Schema
import org.hibernate.annotations.OnDelete
import org.hibernate.annotations.OnDeleteAction
import java.time.Instant
import java.util.*
import javax.persistence.*

@Entity
@Table(indexes = [Index(name = "section_codename_idx", columnList = "codename", unique = true)])
data class Section(
    @Id
    @GeneratedValue
    @Column(nullable = false, length = 36)
    @field:Schema(description = "The section unique identifier")
    override val uuid: UUID = UUID.randomUUID(),

    @Column(nullable = false)
    @field:Schema(description = "The section creation date", example = "2021-04-22T06:00:00Z")
    val creationDate: Instant = Instant.now(),

    @Column(nullable = false, length = 128)
    @field:Schema(description = "The section unique codename")
    val codename: String,

    @Column(nullable = true)
    @field:Schema(description = "The section expiration date", example = "2021-04-22T06:00:00Z")
    override val expirationDate: Instant? = null,

    @JsonIgnore
    @Column(nullable = true, length = 128)
    val cipherKey: String? = null,

    @JsonIgnore
    @ManyToOne
    @JoinColumn(nullable = true)
    @OnDelete(action = OnDeleteAction.CASCADE)
    override var transaction: StorageTransaction? = null

) : BaseEntity, TimeoutEntity, TransactionalEntity