package io.sirius.storage.model

import java.util.*

interface BaseEntity {

    val uuid: UUID
}