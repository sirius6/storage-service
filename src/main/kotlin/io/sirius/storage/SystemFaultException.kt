package io.sirius.storage

class SystemFaultException(msg: String, th: Throwable? = null) : Exception(msg, th)