package io.sirius.storage.repository

import io.sirius.storage.model.StoredFileMetadata
import io.sirius.storage.model.StoredFile
import org.springframework.data.jpa.repository.JpaRepository
import org.springframework.stereotype.Repository
import java.util.*

@Repository
interface StoredFileMetadataRepository : JpaRepository<StoredFileMetadata, UUID> {

    fun deleteByStoredFile(storedFile: StoredFile)

    fun findByStoredFile(storedFile: StoredFile): List<StoredFileMetadata>
}