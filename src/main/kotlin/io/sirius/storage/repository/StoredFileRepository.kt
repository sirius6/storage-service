package io.sirius.storage.repository

import io.sirius.storage.model.Section
import io.sirius.storage.model.StorageTransaction
import io.sirius.storage.model.StoredFile
import org.springframework.data.jpa.repository.EntityGraph
import org.springframework.data.jpa.repository.JpaRepository
import org.springframework.stereotype.Repository
import java.time.Instant
import java.util.*

@Repository
interface StoredFileRepository : JpaRepository<StoredFile, UUID> {

    @EntityGraph("StoredFile.WithMetadata")
    fun findByUuidAndSectionAndTransaction(
        uuid: UUID,
        section: Section,
        transaction: StorageTransaction?
    ): StoredFile?

    fun findByTransaction(transaction: StorageTransaction): List<StoredFile>

    @EntityGraph("StoredFile.WithMetadata")
    fun findBySection(section: Section): List<StoredFile>

    fun findByFilenameAndTransactionAndSectionCodename(
        filename: String,
        transaction: StorageTransaction?,
        sectionCodename: String
    ): StoredFile?

    fun findByExpirationDateBefore(now: Instant): List<StoredFile>

}