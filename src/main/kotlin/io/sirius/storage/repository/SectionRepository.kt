package io.sirius.storage.repository

import io.sirius.storage.model.Section
import io.sirius.storage.model.StorageTransaction
import org.springframework.data.jpa.repository.JpaRepository
import org.springframework.stereotype.Repository
import java.time.Instant
import java.util.*

@Repository
interface SectionRepository : JpaRepository<Section, UUID> {

    fun findByCodename(codename: String): Section?

    fun findByUuidAndTransaction(uuid: UUID, transaction: StorageTransaction?): Section?

    fun findByExpirationDateBefore(now: Instant): List<Section>

    fun findByTransaction(transaction: StorageTransaction): List<Section>
}