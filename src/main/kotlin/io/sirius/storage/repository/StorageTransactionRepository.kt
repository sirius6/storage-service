package io.sirius.storage.repository

import io.sirius.storage.model.StorageTransaction
import org.springframework.data.jpa.repository.JpaRepository
import org.springframework.stereotype.Repository
import java.time.Instant
import java.util.*

@Repository
interface StorageTransactionRepository : JpaRepository<StorageTransaction, UUID> {

    fun findByExpirationDateBefore(date: Instant): List<StorageTransaction>
}