package io.sirius.storage.service.impl

import io.sirius.storage.SectionAlreadyExistsException
import io.sirius.storage.UnknownSectionException
import io.sirius.storage.UnknownTransactionException
import io.sirius.storage.dto.SectionCreateRequest
import io.sirius.storage.io.SectionArchive
import io.sirius.storage.model.Section
import io.sirius.storage.model.StorageTransaction
import io.sirius.storage.repository.SectionRepository
import io.sirius.storage.repository.StorageTransactionRepository
import io.sirius.storage.repository.StoredFileRepository
import io.sirius.storage.service.SectionService
import io.sirius.storage.service.SecurityService
import io.sirius.storage.service.StorageAreaService
import io.sirius.storage.util.findByUuid
import io.sirius.storage.util.logger
import io.sirius.storage.util.mdc
import org.springframework.stereotype.Service
import org.springframework.transaction.annotation.Transactional
import org.springframework.util.FileSystemUtils
import java.util.*

@Service
class SectionServiceImpl(
    private val sectionRepository: SectionRepository,
    private val storedFileRepository: StoredFileRepository,
    private val transactionRepository: StorageTransactionRepository,
    private val storageAreaService: StorageAreaService,
    private val securityServiceImpl: SecurityService
) : SectionService {

    private val logger = logger()

    @Transactional
    override fun createSection(request: SectionCreateRequest): Section {
        // Avoid duplication
        if (sectionRepository.findByCodename(request.codename) != null) {
            throw SectionAlreadyExistsException("A section already exists with the codename ${request.codename}")
        }

        val transaction = request.transactionUuid?.let { getStorageTransaction(it) }

        var section = Section(
            codename = request.codename,
            expirationDate = request.expirationDate,
            cipherKey = if (request.secure) securityServiceImpl.generateCipherKey() else null,
            transaction = transaction
        )
        section.mdc()
        section = sectionRepository.save(section)
        logger.info("New section created")
        return section
    }

    @Transactional(readOnly = true)
    override fun getSection(sectionUuid: UUID, transactionUuid: UUID?): Section {
        val transaction = transactionUuid?.let { getStorageTransaction(it) }
        return sectionRepository.findByUuidAndTransaction(sectionUuid, transaction)?.also { it.mdc() }
            ?: throw UnknownSectionException("No section found with uuid $sectionUuid")
    }

    @Transactional
    override fun deleteSection(sectionUuid: UUID, transactionUuid: UUID?) {
        val section = getSection(sectionUuid, transactionUuid)
        sectionRepository.delete(section) // Cascade files
        FileSystemUtils.deleteRecursively(storageAreaService.getSectionDir(section))
        logger.info("Section deleted with its files")
    }

    @Transactional(readOnly = true)
    override fun getSectionArchive(sectionUuid: UUID, transactionUuid: UUID?): SectionArchive {
        val section = getSection(sectionUuid, transactionUuid)
        val storedFiles = storedFileRepository.findBySection(section)
            .sortedBy { it.filename }

        return SectionArchive(
            section,
            storedFiles.map { storageAreaService.getStoredFileResource(it) }
        )
    }

    private fun getStorageTransaction(transactionUuid: UUID): StorageTransaction =
        transactionRepository.findByUuid(transactionUuid)
            ?: throw UnknownTransactionException("No transaction found with with identifier $transactionUuid")

}