package io.sirius.storage.service.impl

import io.sirius.storage.UnknownTransactionException
import io.sirius.storage.config.StorageProperties
import io.sirius.storage.model.Section
import io.sirius.storage.model.StorageTransaction
import io.sirius.storage.model.StoredFile
import io.sirius.storage.repository.SectionRepository
import io.sirius.storage.repository.StorageTransactionRepository
import io.sirius.storage.repository.StoredFileRepository
import io.sirius.storage.service.StorageTransactionService
import io.sirius.storage.service.StoredFileService
import io.sirius.storage.util.findByUuid
import io.sirius.storage.util.logger
import org.springframework.stereotype.Service
import org.springframework.transaction.annotation.Transactional
import java.time.Instant
import java.time.temporal.ChronoUnit
import java.util.*

@Service
class StorageTransactionServiceImpl(
    private val transactionRepository: StorageTransactionRepository,
    private val storedFileRepository: StoredFileRepository,
    private val sectionRepository: SectionRepository,
    private val storedFileService: StoredFileService,
    private val properties: StorageProperties
) : StorageTransactionService {

    private val logger = logger()

    @Transactional
    override fun createTransaction(): StorageTransaction {
        var transaction = StorageTransaction(
            expirationDate = Instant.now().plus(properties.transactionTimeout, ChronoUnit.MINUTES)
        )
        transaction = transactionRepository.save(transaction)
        logger.info("New transaction ${transaction.uuid} created")
        return transaction
    }

    @Transactional(readOnly = true)
    override fun getStoredFiles(transactionUuid: UUID): List<StoredFile> {
        val transaction = getTransaction(transactionUuid)
        return storedFileRepository.findByTransaction(transaction)
    }

    @Transactional(readOnly = true)
    override fun getSections(transactionUuid: UUID): List<Section> {
        val transaction = getTransaction(transactionUuid)
        return sectionRepository.findByTransaction(transaction)
    }

    @Transactional
    override fun commitTransaction(transactionUuid: UUID) {
        val transaction = getTransaction(transactionUuid)

        val storedFiles = storedFileRepository.findByTransaction(transaction)
        storedFiles.forEach { it.transaction = null }
        storedFileRepository.saveAll(storedFiles)

        val sections = sectionRepository.findByTransaction(transaction)
        sections.forEach { it.transaction = null }
        sectionRepository.saveAll(sections)

        transactionRepository.delete(transaction)
        logger.info("Transaction ${transaction.uuid} committed")
    }

    @Transactional
    override fun rollbackTransaction(transactionUuid: UUID) {
        val transaction = getTransaction(transactionUuid)

        storedFileRepository.findByTransaction(transaction)
            .forEach { storedFileService.deleteFile(it) }
        transactionRepository.delete(transaction)
        logger.info("Transaction ${transaction.uuid} rollback")
    }

    private fun getTransaction(transactionUuid: UUID): StorageTransaction =
        transactionRepository.findByUuid(transactionUuid)
            ?: throw UnknownTransactionException("Unknown transaction $transactionUuid")
}