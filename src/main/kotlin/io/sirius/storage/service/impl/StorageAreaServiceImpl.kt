package io.sirius.storage.service.impl

import io.sirius.storage.config.StorageProperties
import io.sirius.storage.io.SecureStorageAlgorithm
import io.sirius.storage.io.SimpleStorageAlgorithm
import io.sirius.storage.io.StorageAlgorithm
import io.sirius.storage.io.StoredFileResource
import io.sirius.storage.model.Section
import io.sirius.storage.model.StoredFile
import io.sirius.storage.service.SecurityService
import io.sirius.storage.service.StorageAreaService
import org.springframework.stereotype.Service
import java.nio.file.FileSystem
import java.nio.file.Path
import java.util.*

@Service
class StorageAreaServiceImpl(
    private val fileSystem: FileSystem,
    private val properties: StorageProperties,
    private val securityService: SecurityService
) : StorageAreaService {

    override fun getSectionDir(section: Section): Path = fileSystem
        .getPath(properties.directory)
        .resolve("sections")
        .resolve(section.uuid.prefix())
        .resolve(section.uuid.toString())

    override fun getStoredFile(storedFile: StoredFile): Path = getSectionDir(storedFile.section)
        .resolve("files")
        .resolve(storedFile.uuid.prefix())
        .resolve(storedFile.uuid.toString())

    override fun getStoredFileResource(storedFile: StoredFile): StoredFileResource {
        val file = getStoredFile(storedFile)
        val algorithm: StorageAlgorithm = storedFile.section.cipherKey
            ?.let { SecureStorageAlgorithm(securityService.extractCipherKey(it)) }
            ?: SimpleStorageAlgorithm()

        return StoredFileResource(file, storedFile, algorithm)
    }

    /** Avoid having directory with too many files */
    private fun UUID.prefix(): String = this.toString().substring(0, 2)
}