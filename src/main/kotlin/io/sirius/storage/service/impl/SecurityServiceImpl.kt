package io.sirius.storage.service.impl

import io.sirius.storage.service.SecurityService
import io.sirius.storage.util.decrypt
import io.sirius.storage.util.encrypt
import io.sirius.storage.util.logger
import org.springframework.security.crypto.codec.Hex
import org.springframework.stereotype.Service
import javax.crypto.Cipher
import javax.crypto.KeyGenerator
import javax.crypto.SecretKey
import javax.crypto.spec.SecretKeySpec

@Service
class SecurityServiceImpl(private val masterKey: SecretKey) : SecurityService {

    private val logger = logger()

    override fun generateCipherKey(): String {
        // Generate key
        val keyGen = KeyGenerator.getInstance("AES")
        keyGen.init(256)
        val key = keyGen.generateKey()
        logger.info("Cipher key generated")

        // Encrypt key with master key
        val encryptedKey = masterKey.encrypt(key.encoded)
        return Hex.encode(encryptedKey).concatToString()
    }

    override fun extractCipherKey(encryptedKey: String): SecretKey {
        val keyData = masterKey.decrypt(Hex.decode(encryptedKey))
        return SecretKeySpec(keyData, "AES")
    }

}