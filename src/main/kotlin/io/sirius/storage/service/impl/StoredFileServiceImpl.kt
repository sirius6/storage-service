package io.sirius.storage.service.impl

import io.sirius.storage.*
import io.sirius.storage.dto.MetadataUpdateRequest
import io.sirius.storage.dto.StoredFileCreateRequest
import io.sirius.storage.io.StoredFileResource
import io.sirius.storage.model.Section
import io.sirius.storage.model.StorageTransaction
import io.sirius.storage.model.StoredFile
import io.sirius.storage.model.StoredFileMetadata
import io.sirius.storage.repository.SectionRepository
import io.sirius.storage.repository.StorageTransactionRepository
import io.sirius.storage.repository.StoredFileMetadataRepository
import io.sirius.storage.repository.StoredFileRepository
import io.sirius.storage.service.StorageAreaService
import io.sirius.storage.service.StoredFileService
import io.sirius.storage.util.*
import org.apache.tika.config.TikaConfig
import org.apache.tika.io.TemporaryResources
import org.apache.tika.io.TikaInputStream
import org.apache.tika.metadata.Metadata
import org.apache.tika.mime.MimeType
import org.springframework.core.io.InputStreamSource
import org.springframework.http.MediaType
import org.springframework.stereotype.Service
import org.springframework.transaction.annotation.Transactional
import org.springframework.web.multipart.MultipartFile
import java.nio.file.Files
import java.time.Instant
import java.util.*
import kotlin.math.absoluteValue


@Service
class StoredFileServiceImpl(
    private val storedFileRepository: StoredFileRepository,
    private val sectionRepository: SectionRepository,
    private val metadataRepository: StoredFileMetadataRepository,
    private val transactionRepository: StorageTransactionRepository,
    private val storageAreaService: StorageAreaService,
    private val tikaConfig: TikaConfig
) : StoredFileService {

    private val logger = logger()

    @Transactional
    override fun storeFile(
        sectionUuid: UUID,
        request: StoredFileCreateRequest
    ): StoredFile {
        // If the section is transactional, the file to save must be transactional too
        val section = getSection(sectionUuid)
        section.mdc()
        val sectionTransaction = section.transaction
        if (sectionTransaction != null && sectionTransaction.uuid != request.transactionUuid) {
            throw UnknownSectionException("Inconsistent transaction between section and file")
        }

        var storedFile = StoredFile(
            section = section,
            fileSize = request.file.size,
            filename = request.file.getFilename(),
            contentType = request.file.contentType.trimToNull() ?: MediaType.APPLICATION_OCTET_STREAM_VALUE,
            sha256 = request.file.validateHash(request.sha256),
            expirationDate = request.expirationDate,
            transaction = request.transactionUuid?.let { getTransaction(it) }
        )
        storedFile.mdc()
        storedFile = storedFileRepository.save(storedFile)

        val resource = storedFile.toResource()
        request.file.copyTo(resource)
        logger.info("File stored in {}", resource.file)

        return storedFile
    }

    @Transactional
    override fun updateFile(
        sectionUuid: UUID,
        fileUuid: UUID,
        request: StoredFileCreateRequest
    ): StoredFile {
        var storedFile = getStoredFile(sectionUuid, fileUuid, request.transactionUuid)
        storedFile.fileSize = request.file.size
        storedFile.filename = request.file.getFilename()
        storedFile.contentType = request.file.contentType.trimToNull() ?: MediaType.APPLICATION_OCTET_STREAM_VALUE
        storedFile.sha256 = request.file.validateHash(request.sha256)
        storedFile = storedFileRepository.save(storedFile)

        val resource = storedFile.toResource()
        if (!resource.isWritable) {
            throw FileNotWritableException("File is not currently writable")
        }
        request.file.copyTo(resource)
        logger.info("File updated")

        return storedFile
    }

    @Transactional(readOnly = true)
    override fun getSectionStoredFiles(sectionUuid: UUID): List<StoredFile> {
        val section = getSection(sectionUuid)
        return storedFileRepository.findBySection(section)
    }

    @Transactional(readOnly = true)
    override fun getStoredFile(
        sectionUuid: UUID,
        fileUuid: UUID,
        transactionUuid: UUID?
    ): StoredFile {
        val section = getSection(sectionUuid)
        val transaction = transactionUuid?.let { getTransaction(it) }
        return storedFileRepository.findByUuidAndSectionAndTransaction(fileUuid, section, transaction)?.also { it.mdc() }
            ?: throw UnknownStoredFileException("No file found with identifier $fileUuid")
    }

    private fun getSection(sectionUuid: UUID): Section =
        sectionRepository.findByUuid(sectionUuid)
            ?: throw UnknownSectionException("No section found with codename $sectionUuid")

    private fun getTransaction(transactionUuid: UUID): StorageTransaction =
        transactionRepository.findByUuid(transactionUuid)
            ?: throw UnknownTransactionException("Unknown transaction identifier")

    @Transactional
    override fun deleteFile(
        sectionUuid: UUID,
        fileUuid: UUID,
        transactionUuid: UUID?
    ) {
        deleteFile(getStoredFile(sectionUuid, fileUuid, transactionUuid))
    }

    override fun deleteFile(storedFile: StoredFile) {
        Files.deleteIfExists(storedFile.toResource().file)
        storedFileRepository.delete(storedFile)
        logger.info("File deleted")
    }

    @Transactional
    override fun getFileResource(
        sectionUuid: UUID,
        fileUuid: UUID,
        transactionUuid: UUID?
    ): StoredFileResource {
        val storedFile = getStoredFile(sectionUuid, fileUuid, transactionUuid)
        return getFileResource(storedFile)
    }

    @Transactional
    override fun getFileResource(
        sectionCodename: String,
        filename: String
    ): StoredFileResource {
        val storedFile = storedFileRepository.findByFilenameAndTransactionAndSectionCodename(filename, null, sectionCodename)
            ?: throw UnknownStoredFileException("No file found for $sectionCodename/$filename")
        storedFile.mdc()
        return getFileResource(storedFile)
    }

    private fun getFileResource(storedFile: StoredFile): StoredFileResource {
        storedFile.lastDownloadDate = Instant.now()
        storedFileRepository.save(storedFile)
        val resource = storedFile.toResource()
        if (!resource.isReadable) {
            throw FileNotReadableException("File is not currently readable")
        }
        logger.info("File requested for download")
        return resource
    }

    @Transactional
    override fun updateMetadata(
        sectionUuid: UUID,
        fileUuid: UUID,
        request: MetadataUpdateRequest
    ) {
        val storedFile = getStoredFile(sectionUuid, fileUuid, request.transactionUuid)
        storedFile.mdc()

        metadataRepository.deleteByStoredFile(storedFile)
        metadataRepository.saveAll(request.metadata.toMetadata(storedFile))
        logger.info("Metadata updated for stored file")
    }

    private fun StoredFile.toResource(): StoredFileResource =
        storageAreaService.getStoredFileResource(this)

    private fun Map<String, String>.toMetadata(storedFile: StoredFile): List<StoredFileMetadata> =
        map { StoredFileMetadata(name = it.key, value = it.value, storedFile = storedFile) }

    private fun InputStreamSource.copyTo(resource: StoredFileResource) {
        // Ensure parent directory exists
        val parent = resource.file.parent
        if (Files.notExists(parent)) {
            Files.createDirectories(parent)
        }

        // Copy data
        inputStream.use { input ->
            resource.outputStream.use { output ->
                input.copyTo(output)
            }
        }
    }

    /** Get the present filename or create a random filename */
    private fun MultipartFile.getFilename(): String = originalFilename?.trim() ?: generateFilename(this)

    internal fun generateFilename(file: MultipartFile): String {
        val randomValue = String.format("%05d", Random().nextInt(99999).absoluteValue)
        val mimeType = file.getTikaMimeType(tikaConfig)
        return "file-$randomValue${mimeType.extension}"
    }

    private fun MultipartFile.getTikaMimeType(tikaConfig: TikaConfig): MimeType {
        inputStream.use { stream ->
            TemporaryResources().use { resource ->
                val tikaStream = TikaInputStream.get(stream, resource)
                val mediaType = tikaConfig.mimeRepository.detect(tikaStream, Metadata())

                // Fest the most common extension for the detected type
                return tikaConfig.mimeRepository.forName(mediaType.toString())
            }
        }
    }

    private fun InputStreamSource.validateHash(sha256: String?): String {
        val calculatedSha256 = this.sha256()
        if (sha256 != null && !sha256.equals(calculatedSha256, ignoreCase = true)) {
            throw InvalidFileHashException("Invalid file hash")
        }
        return calculatedSha256
    }
}