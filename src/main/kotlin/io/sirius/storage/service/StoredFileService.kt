package io.sirius.storage.service

import io.sirius.storage.dto.MetadataUpdateRequest
import io.sirius.storage.dto.StoredFileCreateRequest
import io.sirius.storage.io.StoredFileResource
import io.sirius.storage.model.StoredFile
import org.springframework.transaction.annotation.Transactional
import java.util.*

interface StoredFileService {

    fun storeFile(
        sectionUuid: UUID,
        request: StoredFileCreateRequest,
    ): StoredFile

    fun updateFile(
        sectionUuid: UUID,
        fileUuid: UUID,
        request: StoredFileCreateRequest
    ): StoredFile

    fun getStoredFile(
        sectionUuid: UUID,
        fileUuid: UUID,
        transactionUuid: UUID? = null
    ): StoredFile

    fun deleteFile(
        sectionUuid: UUID,
        fileUuid: UUID,
        transactionUuid: UUID? = null,
    )

    fun deleteFile(
        storedFile: StoredFile
    )

    fun getFileResource(
        sectionUuid: UUID,
        fileUuid: UUID,
        transactionUuid: UUID? = null,
    ): StoredFileResource

    fun updateMetadata(
        sectionUuid: UUID,
        fileUuid: UUID,
        request: MetadataUpdateRequest
    )

    fun getSectionStoredFiles(
        sectionUuid: UUID
    ): List<StoredFile>

    fun getFileResource(
        sectionCodename: String,
        filename: String
    ): StoredFileResource
}