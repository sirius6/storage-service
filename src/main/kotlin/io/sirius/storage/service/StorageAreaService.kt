package io.sirius.storage.service

import io.sirius.storage.io.StoredFileResource
import io.sirius.storage.model.Section
import io.sirius.storage.model.StoredFile
import java.nio.file.Path

interface StorageAreaService {

    fun getSectionDir(section: Section): Path

    fun getStoredFile(storedFile: StoredFile): Path

    fun getStoredFileResource(storedFile: StoredFile): StoredFileResource
}