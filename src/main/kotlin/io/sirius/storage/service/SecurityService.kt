package io.sirius.storage.service

import javax.crypto.SecretKey

interface SecurityService {

    /** Generate a new section cipher key (encoded in Hex) */
    fun generateCipherKey(): String

    /** Extract a section cipher key from its Hex String */
    fun extractCipherKey(encryptedKey: String): SecretKey
}