package io.sirius.storage.service

import io.sirius.storage.dto.SectionCreateRequest
import io.sirius.storage.io.SectionArchive
import io.sirius.storage.model.Section
import java.util.*

interface SectionService {

    fun createSection(request: SectionCreateRequest): Section

    fun getSection(
        sectionUuid: UUID,
        transactionUuid: UUID?
    ): Section

    fun deleteSection(
        sectionUuid: UUID,
        transactionUuid: UUID?
    )

    fun getSectionArchive(
        sectionUuid: UUID,
        transactionUuid: UUID?
    ): SectionArchive
}