package io.sirius.storage.service

import io.sirius.storage.model.Section
import io.sirius.storage.model.StorageTransaction
import io.sirius.storage.model.StoredFile
import java.util.*

interface StorageTransactionService {

    fun createTransaction(): StorageTransaction

    fun getStoredFiles(transactionUuid: UUID): List<StoredFile>

    fun getSections(transactionUuid: UUID): List<Section>

    fun commitTransaction(transactionUuid: UUID)

    fun rollbackTransaction(transactionUuid: UUID)
}