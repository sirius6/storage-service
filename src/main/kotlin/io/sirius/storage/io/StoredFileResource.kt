package io.sirius.storage.io

import io.sirius.storage.model.StoredFile
import org.springframework.core.io.Resource
import org.springframework.core.io.WritableResource
import org.springframework.http.MediaType
import java.io.*
import java.net.URI
import java.net.URL
import java.nio.file.Files
import java.nio.file.Path
import java.nio.file.StandardCopyOption

class StoredFileResource(
    val file: Path,
    val storedFile: StoredFile,
    private val algorithm: StorageAlgorithm
) : WritableResource {

    /** File used when creating file (atomic) */
    private val tempFile: Path = file.resolveSibling(file.fileName.toString() + ".tmp")

    val mediaType: MediaType = MediaType.valueOf(storedFile.contentType)


    override fun exists(): Boolean = Files.exists(file)

    override fun getURL(): URL = file.toUri().toURL()

    override fun getURI(): URI = file.toUri()

    override fun getFile(): File = file.toFile()

    override fun contentLength(): Long = storedFile.fileSize

    override fun lastModified(): Long = Files.getLastModifiedTime(file).toMillis()

    override fun isFile(): Boolean = true

    override fun createRelative(relativePath: String): Resource {
        throw IOException("Feature not supported")
    }

    override fun getFilename(): String? = storedFile.filename

    override fun getDescription(): String = "StoreFile [${storedFile.uuid}]"

    override fun isReadable(): Boolean = exists() && Files.notExists(tempFile)

    override fun isWritable(): Boolean = Files.notExists(tempFile)

    override fun getInputStream(): InputStream {
        if (!isReadable) {
            throw IOException("File is not currently readable")
        }
        return algorithm.getInputStream(file)
    }

    override fun getOutputStream(): OutputStream {
        if (!isWritable) {
            throw IOException("File is not currently writable")
        }
        Files.createFile(tempFile)
        return AtomicOutputStream(algorithm.getOutputStream(tempFile))
    }

    private inner class AtomicOutputStream(output: OutputStream) : FilterOutputStream(output) {

        override fun close() {
            super.close()
            Files.move(tempFile, file, StandardCopyOption.ATOMIC_MOVE, StandardCopyOption.REPLACE_EXISTING)
        }
    }

}