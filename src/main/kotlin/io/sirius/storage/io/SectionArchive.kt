package io.sirius.storage.io

import io.sirius.storage.model.Section
import io.sirius.storage.model.StoredFile
import org.springframework.web.servlet.mvc.method.annotation.StreamingResponseBody
import java.io.OutputStream
import java.nio.file.attribute.FileTime
import java.time.Instant
import java.util.zip.ZipEntry
import java.util.zip.ZipOutputStream

class SectionArchive(
    section: Section,
    private val resources: List<StoredFileResource>
) : StreamingResponseBody {

    val filename: String = section.codename + ".zip"

    override fun writeTo(outputStream: OutputStream) {
        val zos = ZipOutputStream(outputStream)

        for (resource in resources) {
            zos.putNextEntry(resource.storedFile.toZipEntry())
            resource.inputStream.use { it.copyTo(zos) }
            zos.closeEntry()
        }
        zos.finish()
        zos.flush()
    }

    private fun StoredFile.toZipEntry(): ZipEntry {
        val entry = ZipEntry(filename)
        entry.creationTime = creationDate.toFileTime()
        lastModificationDate?.toFileTime()?.let { entry.lastModifiedTime = it }
        lastDownloadDate?.toFileTime()?.let { entry.lastAccessTime = it }
        entry.size = fileSize
        return entry
    }

    private fun Instant.toFileTime() = FileTime.from(this)

}