package io.sirius.storage.io

import java.io.BufferedInputStream
import java.io.BufferedOutputStream
import java.io.InputStream
import java.io.OutputStream
import java.nio.file.Files
import java.nio.file.Path
import java.security.SecureRandom
import javax.crypto.Cipher
import javax.crypto.CipherInputStream
import javax.crypto.CipherOutputStream
import javax.crypto.SecretKey
import javax.crypto.spec.GCMParameterSpec

interface StorageAlgorithm {
    fun getInputStream(file: Path): InputStream
    fun getOutputStream(file: Path): OutputStream
}

/** Simple algorithm without ciphering */
class SimpleStorageAlgorithm : StorageAlgorithm {
    override fun getInputStream(file: Path): InputStream = Files.newInputStream(file)
    override fun getOutputStream(file: Path): OutputStream = Files.newOutputStream(file)
}

/** Storage using AES/GCM algorithm */
class SecureStorageAlgorithm(private val key: SecretKey) : StorageAlgorithm {

    companion object {
        private const val ALGORITHM = "AES/GCM/NoPadding"
        private const val TAG_LENGTH_BIT = 128
        private const val BUFFER_SIZE = 65_536 // 64KB
    }

    override fun getInputStream(file: Path): InputStream {
        val inputStream = BufferedInputStream(Files.newInputStream(file), BUFFER_SIZE)

        // Read init vector
        val iv = ByteArray(TAG_LENGTH_BIT)
        inputStream.read(iv)

        val cipher = Cipher.getInstance(ALGORITHM)
        cipher.init(Cipher.DECRYPT_MODE, key, GCMParameterSpec(TAG_LENGTH_BIT, iv))
        return CipherInputStream(inputStream, cipher)
    }

    override fun getOutputStream(file: Path): OutputStream {
        val outputStream = BufferedOutputStream(Files.newOutputStream(file), BUFFER_SIZE)

        // Generate init vector
        val iv = ByteArray(TAG_LENGTH_BIT)
        SecureRandom().nextBytes(iv)
        outputStream.write(iv)

        val cipher = Cipher.getInstance(ALGORITHM)
        cipher.init(Cipher.ENCRYPT_MODE, key, GCMParameterSpec(TAG_LENGTH_BIT, iv))
        return CipherOutputStream(outputStream, cipher)
    }
}