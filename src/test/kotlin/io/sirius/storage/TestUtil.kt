package io.sirius.storage

import io.mockk.every
import io.mockk.mockk
import io.sirius.storage.model.BaseEntity
import org.assertj.core.api.Condition
import org.springframework.core.io.Resource
import org.springframework.data.jpa.repository.JpaRepository
import org.springframework.mock.web.MockMultipartFile
import org.springframework.web.multipart.MultipartFile
import java.nio.file.Files
import java.nio.file.Path
import java.util.*

object TestUtil {

    fun Resource.toByteArray(): ByteArray = inputStream.use { it.readAllBytes() }

    inline fun <reified T : JpaRepository<E, UUID>, reified E : BaseEntity> mockRepository(): T {
        val mock = mockk<T>(relaxed = true)
        every { mock.save(any<E>()) } answers { firstArg() }
        return mock
    }

    fun Resource.toMultipartFile(): MultipartFile =
        MockMultipartFile("file", filename, "image/png", this.toByteArray())

    fun MultipartFile.toByteArray(): ByteArray = inputStream.use { it.readAllBytes() }

    fun Path.toByteArray(): ByteArray = Files.newInputStream(this).use { it.readAllBytes() }

    fun MultipartFile.toCondition() = Condition<ByteArray>({ Arrays.equals(it, toByteArray()) }, "byte array compare")
}