package io.sirius.storage.service.impl

import io.sirius.storage.util.decrypt
import io.sirius.storage.util.encrypt
import org.assertj.core.api.Assertions.assertThat
import org.junit.jupiter.api.BeforeEach
import org.junit.jupiter.api.Test
import javax.crypto.KeyGenerator

class SecurityServiceImplTest {

    private lateinit var service: SecurityServiceImpl

    @BeforeEach
    fun before() {
        val keyGen = KeyGenerator.getInstance("AES")
        keyGen.init(128)
        service = SecurityServiceImpl(keyGen.generateKey())
    }

    @Test
    fun testCipherKey() {
        val encodedKey = service.generateCipherKey()
        val key = service.extractCipherKey(encodedKey)

        val data = "Hello dude!!!".toByteArray()
        val encoded = key.encrypt(data)
        assertThat(key.decrypt(encoded)).isEqualTo(data)
    }
}