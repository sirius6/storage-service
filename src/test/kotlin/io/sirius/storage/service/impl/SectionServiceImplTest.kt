package io.sirius.storage.service.impl

import com.google.common.jimfs.Jimfs
import io.mockk.every
import io.mockk.mockk
import io.mockk.verify
import io.sirius.storage.TestUtil.mockRepository
import io.sirius.storage.config.StorageProperties
import io.sirius.storage.dto.SectionCreateRequest
import io.sirius.storage.model.Section
import io.sirius.storage.repository.SectionRepository
import io.sirius.storage.repository.StorageTransactionRepository
import io.sirius.storage.repository.StoredFileRepository
import io.sirius.storage.service.SecurityService
import io.sirius.storage.util.findByUuid
import org.assertj.core.api.Assertions.assertThat
import org.junit.jupiter.api.AfterEach
import org.junit.jupiter.api.BeforeEach
import org.junit.jupiter.api.Test
import java.nio.file.FileSystem

class SectionServiceImplTest {

    private lateinit var fileSystem: FileSystem
    private lateinit var service: SectionServiceImpl
    private lateinit var sectionRepository: SectionRepository
    private lateinit var storedFileRepository: StoredFileRepository
    private lateinit var transactionRepository: StorageTransactionRepository

    @BeforeEach
    fun before() {
        fileSystem = Jimfs.newFileSystem()
        val properties = StorageProperties()
            .apply { directory = "/var" }

        sectionRepository = mockRepository()
        storedFileRepository = mockRepository()
        transactionRepository = mockRepository()
        val securityService = mockk<SecurityService>()
        val storageArea = StorageAreaServiceImpl(fileSystem, properties, securityService)
        service = SectionServiceImpl(sectionRepository, storedFileRepository, transactionRepository, storageArea, securityService)
    }

    @AfterEach
    fun after() {
        fileSystem.close()
    }

    @Test
    fun testCreateSection() {
        every { sectionRepository.findByUuid(any()) } returns null
        every { sectionRepository.findByCodename("codename") } returns null

        val request = SectionCreateRequest("codename", false)
        val section = service.createSection(request)

        assertThat(section.codename).isEqualTo("codename")
        verify(exactly = 1) { sectionRepository.save(section) }
    }

    @Test
    fun testDeleteSection() {
        val section = Section(codename = "codename", cipherKey = null)
        every { sectionRepository.findByUuidAndTransaction(section.uuid, null) } returns section

        service.deleteSection(section.uuid, null)
        verify(exactly = 1) { sectionRepository.delete(section) }
    }
}