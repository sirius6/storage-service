package io.sirius.storage.service.impl

import com.google.common.jimfs.Jimfs
import io.mockk.every
import io.mockk.verify
import io.sirius.storage.InvalidFileHashException
import io.sirius.storage.TestUtil.mockRepository
import io.sirius.storage.TestUtil.toByteArray
import io.sirius.storage.TestUtil.toCondition
import io.sirius.storage.TestUtil.toMultipartFile
import io.sirius.storage.UnknownStoredFileException
import io.sirius.storage.config.StorageProperties
import io.sirius.storage.dto.StoredFileCreateRequest
import io.sirius.storage.model.Section
import io.sirius.storage.model.StoredFile
import io.sirius.storage.repository.SectionRepository
import io.sirius.storage.repository.StorageTransactionRepository
import io.sirius.storage.repository.StoredFileMetadataRepository
import io.sirius.storage.repository.StoredFileRepository
import io.sirius.storage.service.SecurityService
import io.sirius.storage.util.findByUuid
import org.apache.tika.config.TikaConfig
import org.assertj.core.api.Assertions.assertThat
import org.junit.jupiter.api.*
import org.springframework.core.io.ByteArrayResource
import org.springframework.core.io.ClassPathResource
import org.springframework.http.MediaType
import org.springframework.web.multipart.MultipartFile
import java.nio.file.FileSystem
import java.nio.file.Path
import java.util.*
import java.util.regex.Pattern
import javax.crypto.KeyGenerator

class StoredFileServiceImplTest {

    private val multipartFile: MultipartFile = ClassPathResource("/io/sirius/storage/kotlin.png").toMultipartFile()
    private lateinit var service: StoredFileServiceImpl
    private lateinit var storedFileRepository: StoredFileRepository
    private lateinit var sectionRepository: SectionRepository
    private lateinit var transactionRepository: StorageTransactionRepository
    private lateinit var metadataRepository: StoredFileMetadataRepository
    private lateinit var fileSystem: FileSystem
    private lateinit var section: Section
    private lateinit var securityService: SecurityService

    @BeforeEach
    fun before() {
        fileSystem = Jimfs.newFileSystem()
        val properties = StorageProperties()
            .apply { directory = "/var" }

        section = Section(
            codename = "test",
            cipherKey = null
        )

        val keyGen = KeyGenerator.getInstance("AES")
        keyGen.init(128)

        storedFileRepository = mockRepository()
        sectionRepository = mockRepository()
        transactionRepository = mockRepository()
        metadataRepository = mockRepository()
        securityService = SecurityServiceImpl(keyGen.generateKey())
        val storageArea = StorageAreaServiceImpl(fileSystem, properties, securityService)

        service = StoredFileServiceImpl(
            storedFileRepository,
            sectionRepository,
            metadataRepository,
            transactionRepository,
            storageArea,
            TikaConfig.getDefaultConfig()
        )

        every { sectionRepository.findByUuid(section.uuid) } returns section
    }

    @AfterEach
    fun after() {
        fileSystem.close()
    }

    @Nested
    inner class Storage {

        @Test
        fun testSaveFile() {
            val storedFile = service.storeFile(
                sectionUuid = section.uuid,
                request = StoredFileCreateRequest(multipartFile)
            )

            // Check real file
            val file = storedFile.getFile()
            assertThat(file).exists()
            assertThat(file).isRegularFile
            assertThat(file).hasBinaryContent(multipartFile.toByteArray())

            verify(exactly = 1) { storedFileRepository.save(storedFile) }
        }

        @Test
        fun testSaveSecureFile() {
            section = section.copy(cipherKey = securityService.generateCipherKey())
            every { sectionRepository.findByUuid(section.uuid) } returns section

            val storedFile = service.storeFile(
                sectionUuid = section.uuid,
                request = StoredFileCreateRequest(multipartFile)
            )

            // Check real file
            val file = storedFile.getFile()
            assertThat(file).exists()
            assertThat(file).isRegularFile
            assertThat(file.toByteArray()).isNot(multipartFile.toCondition())

            // Read file
            every { storedFileRepository.findByUuidAndSectionAndTransaction(storedFile.uuid, section, null) } returns storedFile
            val resource = service.getFileResource(section.uuid, storedFile.uuid)
            assertThat(resource.toByteArray()).`is`(multipartFile.toCondition())
        }

        @Test
        fun testUpdateFile() {
            val storedFile = StoredFile(
                fileSize = 10_000,
                filename = "test.jpg",
                contentType = "image/png",
                sha256 = "53ff2fcb614f9549ed99858386b92f1cddbe39e163567edfb7b0199c4083be09",
                section = section
            )
            every { storedFileRepository.findByUuidAndSectionAndTransaction(storedFile.uuid, section, null) } returns storedFile

            service.updateFile(
                fileUuid = storedFile.uuid,
                sectionUuid = section.uuid,
                request = StoredFileCreateRequest(ByteArrayResource("Test data 2".toByteArray()).toMultipartFile())
            )

            verify(exactly = 1) { storedFileRepository.save(storedFile) }

            // Check real file
            val file = storedFile.getFile()
            assertThat(file).exists()
            assertThat(file).hasBinaryContent("Test data 2".toByteArray())
        }

        @Test
        fun testDeleteFile() {
            val storedFile = service.storeFile(
                sectionUuid = section.uuid,
                request = StoredFileCreateRequest(multipartFile)
            )
            every { storedFileRepository.findByUuidAndSectionAndTransaction(storedFile.uuid, section, null) } returns storedFile

            assertThat(storedFile.getFile()).exists()

            service.deleteFile(section.uuid, storedFile.uuid)
            assertThat(storedFile.getFile()).doesNotExist()
            verify(exactly = 1) { storedFileRepository.delete(storedFile) }
        }

        private fun StoredFile.getFile(): Path = fileSystem
            .getPath("/var/sections/${section.uuid}/files/${uuid}")
    }

    @Nested
    inner class Download {

        @Test
        fun testDownloadFile() {
            val storedFile = service.storeFile(
                sectionUuid = section.uuid,
                request = StoredFileCreateRequest(multipartFile)
            )

            every { storedFileRepository.findByUuidAndSectionAndTransaction(storedFile.uuid, section, null) } returns storedFile

            assertThat(storedFile.lastDownloadDate).isNull()
            val storedFileResource = service.getFileResource(section.uuid, storedFile.uuid)
            assertThat(storedFile.lastDownloadDate).isNotNull()

            assertThat(storedFileResource.mediaType).isEqualTo(MediaType.IMAGE_PNG)
            assertThat(storedFileResource.filename).isEqualTo("kotlin.png")
            assertThat(storedFileResource.contentLength()).isEqualTo(4_870)
            assertThat(storedFileResource.toByteArray()).isEqualTo(multipartFile.toByteArray())
        }

        @Test
        fun testDownloadUnknownFile() {
            every { storedFileRepository.findByUuidAndSectionAndTransaction(any(), section, null) } returns null
            assertThrows<UnknownStoredFileException> { service.getFileResource(section.uuid, UUID.randomUUID()) }
        }
    }

    @Nested
    inner class HashValidation {

        @Test
        fun testNoHashProvided() {
            val storedFile = service.storeFile(
                sectionUuid = section.uuid,
                request = StoredFileCreateRequest(multipartFile)
            )
            assertThat(storedFile.sha256).isEqualTo("53ff2fcb614f9549ed99858386b92f1cddbe39e163567edfb7b0199c4083be09")
        }

        @Test
        fun testProvidedHashLowerCase() {
            val storedFile = service.storeFile(
                sectionUuid = section.uuid,
                request = StoredFileCreateRequest(
                    file = multipartFile,
                    sha256 = "53ff2fcb614f9549ed99858386b92f1cddbe39e163567edfb7b0199c4083be09"
                )
            )
            assertThat(storedFile.sha256).isEqualTo("53ff2fcb614f9549ed99858386b92f1cddbe39e163567edfb7b0199c4083be09")
        }

        @Test
        fun testProvidedHashUpperCase() {
            val storedFile = service.storeFile(
                sectionUuid = section.uuid,
                request = StoredFileCreateRequest(
                    file = multipartFile,
                    sha256 = "53FF2FCB614F9549ED99858386B92F1CDDBE39E163567EDFB7B0199C4083BE09"
                )
            )
            assertThat(storedFile.sha256).isEqualTo("53ff2fcb614f9549ed99858386b92f1cddbe39e163567edfb7b0199c4083be09")
        }

        @Test
        fun testInvalidHash() {
            assertThrows<InvalidFileHashException> {
                service.storeFile(
                    sectionUuid = section.uuid,
                    request = StoredFileCreateRequest(
                        file = multipartFile,
                        sha256 = "53ff2fcb614f9549ed99858386b92f1cddbe39e163567edfb7b0199c4083be00"
                    )
                )
            }
        }
    }

    @Nested
    inner class Filename {

        @Test
        fun testGeneratedFilename() {
            assertThat(service.generateFilename(multipartFile)).matches(Pattern.compile("^file-\\d{5}.png$"))
        }

    }

}