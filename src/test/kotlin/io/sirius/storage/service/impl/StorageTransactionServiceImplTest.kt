package io.sirius.storage.service.impl

import io.mockk.every
import io.mockk.mockk
import io.mockk.verify
import io.sirius.storage.TestUtil.mockRepository
import io.sirius.storage.config.StorageProperties
import io.sirius.storage.model.Section
import io.sirius.storage.model.StoredFile
import io.sirius.storage.repository.SectionRepository
import io.sirius.storage.repository.StorageTransactionRepository
import io.sirius.storage.repository.StoredFileRepository
import io.sirius.storage.service.StoredFileService
import io.sirius.storage.util.findByUuid
import org.assertj.core.api.Assertions.assertThat
import org.junit.jupiter.api.BeforeEach
import org.junit.jupiter.api.Test

class StorageTransactionServiceImplTest {

    private lateinit var transactionService: StorageTransactionServiceImpl
    private lateinit var storedFileService: StoredFileService
    private lateinit var transactionRepository: StorageTransactionRepository
    private lateinit var storedFileRepository: StoredFileRepository
    private lateinit var sectionRepository: SectionRepository

    private lateinit var section: Section

    @BeforeEach
    fun before() {
        section = Section(codename = "test", cipherKey = null)

        transactionRepository = mockRepository()
        storedFileRepository = mockRepository()
        sectionRepository = mockRepository()
        storedFileService = mockk(relaxed = true)

        transactionService = StorageTransactionServiceImpl(
            transactionRepository,
            storedFileRepository,
            sectionRepository,
            storedFileService,
            StorageProperties()
        )
    }

    @Test
    fun testTransactionCommit() {
        val transaction = transactionService.createTransaction()
        verify(exactly = 1) { transactionRepository.save(transaction) }
        every { transactionRepository.findByUuid(transaction.uuid) } returns transaction

        val storedFile1 = StoredFile(fileSize = 10, filename = "test.jpg", sha256 = "123", contentType = "image/jpg", section = section, transaction = transaction)
        val storedFile2 = StoredFile(fileSize = 10, filename = "test.jpg", sha256 = "123", contentType = "image/jpg", section = section, transaction = transaction)
        every { storedFileRepository.findByTransaction(transaction) } returns listOf(storedFile1, storedFile2)

        transactionService.commitTransaction(transaction.uuid)

        verify(exactly = 1) { transactionRepository.delete(transaction) }
        verify(exactly = 0) { storedFileService.deleteFile(storedFile1) }
        verify(exactly = 0) { storedFileService.deleteFile(storedFile2) }
        assertThat(storedFile1.transaction).isNull()
        assertThat(storedFile2.transaction).isNull()
    }

    @Test
    fun testTransactionRollback() {
        val transaction = transactionService.createTransaction()
        verify(exactly = 1) { transactionRepository.save(transaction) }
        every { transactionRepository.findByUuid(transaction.uuid) } returns transaction

        val storedFile1 = StoredFile(fileSize = 10, filename = "test.jpg", sha256 = "123", contentType = "image/jpg", section = section, transaction = transaction)
        val storedFile2 = StoredFile(fileSize = 10, filename = "test.jpg", sha256 = "123", contentType = "image/jpg", section = section, transaction = transaction)
        every { storedFileRepository.findByTransaction(transaction) } returns listOf(storedFile1, storedFile2)

        transactionService.rollbackTransaction(transaction.uuid)

        verify(exactly = 1) { transactionRepository.delete(transaction) }
        verify(exactly = 1) { storedFileService.deleteFile(storedFile1) }
        verify(exactly = 1) { storedFileService.deleteFile(storedFile2) }
    }
}