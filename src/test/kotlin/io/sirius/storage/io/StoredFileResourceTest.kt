package io.sirius.storage.io

import com.google.common.jimfs.Jimfs
import io.sirius.storage.model.Section
import io.sirius.storage.model.StoredFile
import org.assertj.core.api.Assertions.assertThat
import org.junit.jupiter.api.AfterEach
import org.junit.jupiter.api.BeforeEach
import org.junit.jupiter.api.Test
import org.junit.jupiter.api.assertThrows
import java.io.IOException
import java.nio.file.FileSystem

class StoredFileResourceTest {

    private lateinit var fileSystem: FileSystem
    private lateinit var data: ByteArray
    private lateinit var resource: StoredFileResource

    @BeforeEach
    fun before() {
        data = "Test data".toByteArray()

        val section = Section(
            codename = "test",
            cipherKey = null
        )

        val storedField = StoredFile(
            fileSize = 6,
            filename = "image.jpg",
            contentType = "image/jpeg",
            sha256 = "53ff2fcb614f9549ed99858386b92f1cddbe39e163567edfb7b0199c4083be09",
        section = section
        )

        fileSystem = Jimfs.newFileSystem()
        resource = StoredFileResource(
            file = fileSystem.getPath("/image.jpg"),
            storedFile = storedField,
            algorithm = SimpleStorageAlgorithm()
        )
    }

    @AfterEach
    fun after() {
        fileSystem.close()
    }

    @Test
    fun testWrite() {
        assertThat(fileSystem.getPath("/image.jpg")).doesNotExist()
        assertThat(fileSystem.getPath("/image.jpg.tmp")).doesNotExist()

        val output = resource.outputStream
        assertThat(fileSystem.getPath("/image.jpg.tmp")).exists()
        output.write(data)
        output.close()

        assertThat(fileSystem.getPath("/image.jpg.tmp")).doesNotExist()
        assertThat(fileSystem.getPath("/image.jpg")).exists()
        assertThat(fileSystem.getPath("/image.jpg")).hasBinaryContent(data)
    }

    @Test
    fun testParallelWrite() {
        assertThat(resource.isWritable).isTrue // No file exists and so can be written
        assertThat(resource.isReadable).isFalse // File does not exists

        val output = resource.outputStream

        assertThat(resource.isWritable).isFalse
        assertThat(resource.isReadable).isFalse

        assertThrows<IOException> { resource.outputStream }
        assertThrows<IOException> { resource.inputStream }
        output.close()

        assertThat(resource.isWritable).isTrue
        assertThat(resource.isReadable).isTrue
    }

    @Test
    fun testParallelRead() {
        resource.outputStream.use { it.write(data) }

        val input1 = resource.inputStream
        val input2 = resource.inputStream

        assertThat(input1.readAllBytes()).isEqualTo(data)
        assertThat(input2.readAllBytes()).isEqualTo(data)

        input1.close()
        input2.close()
    }
}