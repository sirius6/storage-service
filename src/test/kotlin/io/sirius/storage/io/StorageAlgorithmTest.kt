package io.sirius.storage.io

import com.google.common.jimfs.Jimfs
import org.assertj.core.api.Assertions.assertThat
import org.junit.jupiter.api.AfterEach
import org.junit.jupiter.api.BeforeEach
import org.junit.jupiter.api.Test
import java.nio.file.FileSystem
import javax.crypto.KeyGenerator

class StorageAlgorithmTest {

    private lateinit var fileSystem: FileSystem

    @BeforeEach
    fun before() {
        fileSystem = Jimfs.newFileSystem()
    }

    @AfterEach
    fun after() {
        fileSystem.close()
    }

    @Test
    fun testSecureStorage() {
        val keyGen = KeyGenerator.getInstance("AES")
        keyGen.init(256)
        val key = keyGen.generateKey()

        val algorithm: StorageAlgorithm = SecureStorageAlgorithm(key)

        val file = fileSystem.getPath("/test.txt")
        algorithm.getOutputStream(file).use { it.write("Hello dude!!!".toByteArray()) }

        val data = algorithm.getInputStream(file).use { it.readAllBytes() }
        assertThat(data).isEqualTo("Hello dude!!!".toByteArray())
    }
}