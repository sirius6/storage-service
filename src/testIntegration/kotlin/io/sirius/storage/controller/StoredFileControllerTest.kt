package io.sirius.storage.controller

import io.sirius.storage.AbstractTest
import io.sirius.storage.TestExtensions.httpMethod
import io.sirius.storage.TestExtensions.payload
import io.sirius.storage.TestExtensions.text
import io.sirius.storage.TestExtensions.toByteArray
import io.sirius.storage.dto.MetadataUpdateRequest
import io.sirius.storage.dto.SectionCreateRequest
import io.sirius.storage.dto.StoredFileDetails
import io.sirius.storage.dto.ErrorPayload
import io.sirius.storage.model.Section
import io.sirius.storage.util.findByUuid
import org.assertj.core.api.Assertions.assertThat
import org.junit.jupiter.api.BeforeEach
import org.junit.jupiter.api.Nested
import org.junit.jupiter.api.Test
import org.springframework.core.io.ClassPathResource
import org.springframework.core.io.Resource
import org.springframework.mock.web.MockMultipartFile
import org.springframework.security.test.web.servlet.request.SecurityMockMvcRequestPostProcessors.httpBasic
import org.springframework.test.web.servlet.request.MockMvcRequestBuilders.*
import org.springframework.test.web.servlet.result.MockMvcResultMatchers.status
import java.util.*

class StoredFileControllerTest : AbstractTest() {

    private val resource: Resource = ClassPathResource("/io/sirius/storage/kotlin.png")
    private lateinit var partFile: MockMultipartFile

    @BeforeEach
    fun before() {
        val rand = Random().nextInt(1000)
        partFile = MockMultipartFile("file", "kotlin-$rand.png", "image/png", resource.toByteArray())
    }

    @Nested
    inner class CipheredData {

        private lateinit var section: Section

        @BeforeEach
        fun before() {
            section = sectionRepository.findByCodename("ciphered")
                ?: createSection(SectionCreateRequest("ciphered", true))
        }

        @Test
        fun testStoreFile() {
            // Save file
            val storedFile: StoredFileDetails = mockMvc.perform(
                multipart("/api/section/${section.uuid}/file")
                    .file(partFile)
                    .text("sha256", "53ff2fcb614f9549ed99858386b92f1cddbe39e163567edfb7b0199c4083be09")
                    .with(httpBasic("sirius", "sirius"))
            ).andExpect(status().isOk)
                .andReturn()
                .payload(objectMapper)

            assertThat(storedFile.getStoreFilePath()).exists()

            // Get file
            val readStoredFile: StoredFileDetails = mockMvc.perform(
                get("/api/section/${section.uuid}/file/${storedFile.uuid}")
                    .with(httpBasic("sirius", "sirius"))
            ).andExpect(status().isOk)
                .andReturn()
                .payload(objectMapper)
            assertThat(storedFile).isEqualTo(readStoredFile)

            // Download file
            val fileData: ByteArray = mockMvc.perform(
                get("/api/section/${section.uuid}/file/${storedFile.uuid}/download")
                    .with(httpBasic("sirius", "sirius"))
            ).andExpect(status().isOk)
                .andReturn().response.contentAsByteArray
            assertThat(resource.toByteArray()).isEqualTo(fileData)
        }
    }

    @Nested
    inner class UnCipheredData {

        private lateinit var section: Section

        @BeforeEach
        fun before() {
            section = sectionRepository.findByCodename("unciphered")
                ?: createSection(SectionCreateRequest("unciphered", false))
        }

        @Test
        fun testMissingAuthentication() {
            mockMvc.perform(
                multipart("/api/section/${section.uuid}/file")
                    .file(partFile)
            ).andExpect(status().isForbidden)
        }

        @Test
        fun testInvalidAuthentication() {
            mockMvc.perform(
                multipart("/api/section/${section.uuid}/file")
                    .file(partFile)
                    .with(httpBasic("sirius", "invalid"))
            ).andExpect(status().isForbidden)
        }

        @Test
        fun testInvalidParameter() {
            val error: ErrorPayload = mockMvc.perform(
                multipart("/api/section/05749cda-9456-11eb-b4c1-e7b628627b11/file")
                    .file(partFile)
                    .text("sha256", "53ff2fcb614f9549ed99858386b92f1cddbe39e163567edfb7b0199c4083be09")
                    .with(httpBasic("sirius", "sirius"))
            ).andExpect(status().isBadRequest)
                .andReturn()
                .payload(objectMapper)

            assertThat(error.message).isEqualTo("No section found with codename 05749cda-9456-11eb-b4c1-e7b628627b11")
        }

        @Test
        fun testStoreFile() {
            // Save file
            val storedFile: StoredFileDetails = mockMvc.perform(
                multipart("/api/section/${section.uuid}/file")
                    .file(partFile)
                    .text("sha256", "53ff2fcb614f9549ed99858386b92f1cddbe39e163567edfb7b0199c4083be09")
                    .with(httpBasic("sirius", "sirius"))
            ).andExpect(status().isOk)
                .andReturn()
                .payload(objectMapper)

            assertThat(storedFile.getStoreFilePath()).exists()

            // Get file
            val readStoredFile: StoredFileDetails = mockMvc.perform(
                get("/api/section/${section.uuid}/file/${storedFile.uuid}")
                    .with(httpBasic("sirius", "sirius"))
            ).andExpect(status().isOk)
                .andReturn()
                .payload(objectMapper)
            assertThat(storedFile).isEqualTo(readStoredFile)

            // Download file
            val fileData: ByteArray = mockMvc.perform(
                get("/api/section/${section.uuid}/file/${storedFile.uuid}/download")
                    .with(httpBasic("sirius", "sirius"))
            ).andExpect(status().isOk)
                .andReturn().response.contentAsByteArray
            assertThat(resource.toByteArray()).isEqualTo(fileData)
        }

        @Test
        fun testInvalidHash() {
            mockMvc.perform(
                multipart("/api/section/${section.uuid}/file")
                    .file(partFile)
                    .text("sha256", "53ff2fcb614f9549ed99858386b92f1cddbe39e163567edfb7b0199c4083be00")
                    .with(httpBasic("sirius", "sirius"))
            ).andExpect(status().isBadRequest)
        }

        @Test
        fun testUpdateFile() {
            // Save file
            val storedFile: StoredFileDetails = mockMvc.perform(
                multipart("/api/section/${section.uuid}/file")
                    .file(partFile)
                    .with(httpBasic("sirius", "sirius"))
            ).andExpect(status().isOk)
                .andReturn()
                .payload(objectMapper)

            // Update file
            mockMvc.perform(
                multipart("/api/section/${section.uuid}/file/${storedFile.uuid}")
                    .file("file", "Test data 2".toByteArray())
                    .with(httpMethod("PUT"))
                    .with(httpBasic("sirius", "sirius"))
            ).andExpect(status().isOk)

            // Download file
            val fileData: ByteArray = mockMvc.perform(
                get("/api/section/${section.uuid}/file/${storedFile.uuid}/download")
                    .with(httpBasic("sirius", "sirius"))
            ).andExpect(status().isOk)
                .andReturn().response.contentAsByteArray
            assertThat("Test data 2".toByteArray()).isEqualTo(fileData)
        }

        @Test
        fun testDeleteFile() {
            // Save file
            val storedFile: StoredFileDetails = mockMvc.perform(
                multipart("/api/section/${section.uuid}/file")
                    .file(partFile)
                    .with(httpBasic("sirius", "sirius"))
            ).andExpect(status().isOk)
                .andReturn()
                .payload(objectMapper)

            val file = storedFile.getStoreFilePath()
            assertThat(file).exists()

            // Delete file
            mockMvc.perform(
                delete("/api/section/${section.uuid}/file/${storedFile.uuid}")
                    .with(httpBasic("sirius", "sirius"))
            ).andExpect(status().isOk)

            assertThat(file).doesNotExist()

            // Get file
            mockMvc.perform(
                get("/api/section/${section.uuid}/file/${storedFile.uuid}")
                    .with(httpBasic("sirius", "sirius"))
            ).andExpect(status().isBadRequest)

            // Download file
            mockMvc.perform(
                get("/api/section/${section.uuid}/file/${storedFile.uuid}/download")
                    .with(httpBasic("sirius", "sirius"))
            ).andExpect(status().isBadRequest)
        }
    }

    @Nested
    inner class StoredFileMetadata {

        private lateinit var section: Section

        @BeforeEach
        fun before() {
            section = sectionRepository.findByCodename("unciphered")
                ?: createSection(SectionCreateRequest("unciphered", false))
        }

        @Test
        fun testFileMetadata() {
            val storedFile: StoredFileDetails = mockMvc.perform(
                multipart("/api/section/${section.uuid}/file")
                    .file(partFile)
                    .with(httpBasic("sirius", "sirius"))
            ).andExpect(status().isOk)
                .andReturn()
                .payload(objectMapper)

            // Update
            mockMvc.perform(
                put("/api/section/${section.uuid}/file/${storedFile.uuid}/metadata")
                    .json(MetadataUpdateRequest(mapOf("key1" to "value1")))
                    .with(httpBasic("sirius", "sirius"))
            ).andExpect(status().isOk)

            val file = storedFileRepository.findByUuid(storedFile.uuid)!!
            assertThat(metadataRepository.findByStoredFile(file))
                .hasSize(1)
                .anyMatch { it.name == "key1" && it.value == "value1" }

            // Another update
            mockMvc.perform(
                put("/api/section/${section.uuid}/file/${storedFile.uuid}/metadata")
                    .json(MetadataUpdateRequest(mapOf("key2" to "value2", "key3" to "value3")))
                    .with(httpBasic("sirius", "sirius"))
            ).andExpect(status().isOk)

            assertThat(metadataRepository.findByStoredFile(file))
                .hasSize(2)
                .anyMatch { it.name == "key2" && it.value == "value2" }
                .anyMatch { it.name == "key3" && it.value == "value3" }

            // Get file
            val readStoredFile: StoredFileDetails = mockMvc.perform(
                get("/api/section/${section.uuid}/file/${storedFile.uuid}")
                    .with(httpBasic("sirius", "sirius"))
            ).andExpect(status().isOk)
                .andReturn()
                .payload(objectMapper)
            assertThat(readStoredFile.metadata).isEqualTo(mapOf("key2" to "value2", "key3" to "value3"))
        }
    }
}