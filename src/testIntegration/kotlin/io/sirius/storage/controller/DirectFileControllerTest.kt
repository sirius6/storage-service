package io.sirius.storage.controller

import io.sirius.storage.AbstractTest
import io.sirius.storage.TestExtensions.payload
import io.sirius.storage.TestExtensions.toByteArray
import io.sirius.storage.dto.SectionCreateRequest
import io.sirius.storage.dto.StoredFileDetails
import io.sirius.storage.model.Section
import org.assertj.core.api.Assertions.assertThat
import org.junit.jupiter.api.BeforeEach
import org.junit.jupiter.api.Test
import org.springframework.core.io.ClassPathResource
import org.springframework.core.io.Resource
import org.springframework.mock.web.MockMultipartFile
import org.springframework.security.test.web.servlet.request.SecurityMockMvcRequestPostProcessors.httpBasic
import org.springframework.test.web.servlet.request.MockMvcRequestBuilders.get
import org.springframework.test.web.servlet.request.MockMvcRequestBuilders.multipart
import org.springframework.test.web.servlet.result.MockMvcResultMatchers.status
import java.net.URI
import java.util.*

class DirectFileControllerTest : AbstractTest() {

    private val resource: Resource = ClassPathResource("/io/sirius/storage/kotlin.png")
    private lateinit var partFile: MockMultipartFile
    private lateinit var section: Section

    @BeforeEach
    fun before() {
        val rand = Random().nextInt(1000)
        partFile = MockMultipartFile("file", "kotlin-$rand.png", "image/png", resource.toByteArray())

        section = sectionRepository.findByCodename("unciphered")
            ?: createSection(SectionCreateRequest("unciphered", false))
    }

    @Test
    fun testStoreFile() {
        // Save file
        val storedFile: StoredFileDetails = mockMvc.perform(
            multipart("/api/section/${section.uuid}/file")
                .file(partFile)
                .with(httpBasic("sirius", "sirius"))
        ).andExpect(status().isOk)
            .andReturn()
            .payload(objectMapper)

        assertThat(storedFile.getStoreFilePath()).exists()
        assertThat(storedFile.directUri).isEqualTo(URI.create("/direct/${section.codename}/${storedFile.filename}"))

        // Get file
        val readData: ByteArray = mockMvc.perform(
            get(storedFile.directUri)
                .with(httpBasic("sirius", "sirius"))
        ).andExpect(status().isOk)
            .andReturn()
            .response
            .contentAsByteArray
        assertThat(resource.toByteArray()).isEqualTo(readData)
    }

    @Test
    fun testUnknownFileDownload() {
        mockMvc.perform(
            get("/direct/${section.codename}/test.jpg")
                .with(httpBasic("sirius", "sirius"))
        ).andExpect(status().isBadRequest)
    }
}