package io.sirius.storage.controller

import io.sirius.storage.AbstractTest
import io.sirius.storage.TestExtensions.payload
import io.sirius.storage.dto.MetadataUpdateRequest
import io.sirius.storage.dto.SectionCreateRequest
import io.sirius.storage.dto.StoredFileDetails
import io.sirius.storage.model.Section
import io.sirius.storage.util.findByUuid
import org.assertj.core.api.Assertions.assertThat
import org.junit.jupiter.api.AfterEach
import org.junit.jupiter.api.BeforeEach
import org.junit.jupiter.api.Test
import org.springframework.mock.web.MockMultipartFile
import org.springframework.security.test.web.servlet.request.SecurityMockMvcRequestPostProcessors.httpBasic
import org.springframework.test.web.servlet.request.MockMvcRequestBuilders.*
import org.springframework.test.web.servlet.result.MockMvcResultMatchers.status
import org.springframework.test.web.servlet.result.MockMvcResultMatchers.request
import org.springframework.util.FileSystemUtils
import java.io.File
import java.nio.file.Files
import java.nio.file.Paths
import java.util.zip.ZipFile

class SectionControllerTest : AbstractTest() {

    private lateinit var tempDir: File

    @BeforeEach
    fun before() {
        tempDir = Files.createTempDirectory("sirius-").toFile()
    }

    @AfterEach
    fun after() {
        FileSystemUtils.deleteRecursively(tempDir)
    }

    @Test
    fun testCreateSection() {
        val section: Section = mockMvc.perform(
            post("/api/section")
                .json(SectionCreateRequest("section", false))
                .with(httpBasic("sirius", "sirius"))
        ).andExpect(status().isOk)
            .andReturn()
            .payload(objectMapper)

        assertThat(section.codename).isEqualTo("section")
        assertThat(sectionRepository.findByUuid(section.uuid)).isEqualTo(section)

        // Get section
        val readSection: Section = mockMvc.perform(
            get("/api/section/${section.uuid}")
                .with(httpBasic("sirius", "sirius"))
        ).andExpect(status().isOk)
            .andReturn()
            .payload(objectMapper)
        assertThat(section).isEqualTo(readSection)

        // Delete section
        mockMvc.perform(
            delete("/api/section/${section.uuid}")
                .with(httpBasic("sirius", "sirius"))
        ).andExpect(status().isOk)
        assertThat(sectionRepository.findByUuid(section.uuid)).isNull()
    }

    @Test
    fun testSectionArchive() {
        val section: Section = mockMvc.perform(
            post("/api/section")
                .json(SectionCreateRequest("section-archive", false))
                .with(httpBasic("sirius", "sirius"))
        ).andExpect(status().isOk)
            .andReturn()
            .payload(objectMapper)

        section.storeFile(MockMultipartFile("file", "file1.txt", "text/plain", "hello".toByteArray()))
        section.storeFile(MockMultipartFile("file", "file2.txt", "text/plain", "guten tag".toByteArray()))
        section.storeFile(MockMultipartFile("file", "file3.txt", "text/plain", "bonjour".toByteArray()))
            .setMetadata(section, mapOf("key1" to "value1"))

        val archiveData = mockMvc.perform(
            get("/api/section/${section.uuid}/archive")
                .with(httpBasic("sirius", "sirius"))
        )
            .andExpect(request().asyncStarted())
            .andDo { it.asyncResult }
            .andExpect(status().isOk)
            .andReturn()
            .response.contentAsByteArray

        val archiveFile = File(tempDir, "archive.zip")
        Files.write(archiveFile.toPath(), archiveData)

        val zipFile = ZipFile(archiveFile, Charsets.UTF_8)
        assertThat(zipFile.stream().map { it.name }).containsExactly("file1.txt", "file2.txt", "file3.txt")
        assertThat(zipFile.readEntry("file1.txt")).isEqualTo("hello")
        assertThat(zipFile.readEntry("file2.txt")).isEqualTo("guten tag")
        assertThat(zipFile.readEntry("file3.txt")).isEqualTo("bonjour")
    }

    private fun Section.storeFile(file: MockMultipartFile): StoredFileDetails = mockMvc.perform(
        multipart("/api/section/$uuid/file")
            .file(file)
            .with(httpBasic("sirius", "sirius"))
    ).andExpect(status().isOk)
        .andReturn()
        .payload(objectMapper)

    private fun StoredFileDetails.setMetadata(
        section: Section,
        metadata: Map<String, String>
    ) {
        mockMvc.perform(
            put("/api/section/${section.uuid}/file/$uuid/metadata")
                .json(MetadataUpdateRequest(metadata))
                .with(httpBasic("sirius", "sirius"))
        ).andExpect(status().isOk)
    }

    private fun ZipFile.readEntry(name: String): String {
        val entry = getEntry(name)
        return getInputStream(entry).use { it.readAllBytes().decodeToString() }
    }
}