package io.sirius.storage.controller

import io.sirius.storage.AbstractTest
import io.sirius.storage.TestExtensions.payload
import io.sirius.storage.TestExtensions.text
import io.sirius.storage.dto.SectionCreateRequest
import io.sirius.storage.dto.StoredFileDetails
import io.sirius.storage.model.Section
import io.sirius.storage.model.StorageTransaction
import org.assertj.core.api.Assertions.assertThat
import org.junit.jupiter.api.BeforeEach
import org.junit.jupiter.api.Nested
import org.junit.jupiter.api.Test
import org.springframework.mock.web.MockMultipartFile
import org.springframework.security.test.web.servlet.request.SecurityMockMvcRequestPostProcessors.httpBasic
import org.springframework.test.web.servlet.request.MockMvcRequestBuilders.*
import org.springframework.test.web.servlet.result.MockMvcResultMatchers.status
import java.util.*

class TransactionControllerTest : AbstractTest() {

    private lateinit var partFile: MockMultipartFile
    private lateinit var transaction: StorageTransaction

    @BeforeEach
    fun before() {
        val rand = Random().nextInt(1000)
        partFile = MockMultipartFile("file", "kotlin-$rand.png", "image/png", "test".toByteArray())

        transaction = mockMvc.perform(
            post("/api/transaction")
                .with(httpBasic("sirius", "sirius"))
                .json(StorageTransaction())
        ).andExpect(status().isOk)
            .andReturn()
            .payload(objectMapper)
    }

    @Nested
    inner class TransactionalFile {

        private lateinit var section: Section

        @BeforeEach
        fun before() {
            section = sectionRepository.findByCodename("ciphered")
                ?: createSection(SectionCreateRequest("ciphered", true))
        }

        @Test
        fun testCommit() {
            assertThat(listStoredFiles(transaction)).isEmpty()

            // Save file
            val storedFile: StoredFileDetails = mockMvc.perform(
                multipart("/api/section/${section.uuid}/file")
                    .file(partFile)
                    .text("transactionUuid", transaction.uuid.toString())
                    .with(httpBasic("sirius", "sirius"))
            ).andExpect(status().isOk)
                .andReturn()
                .payload(objectMapper)

            assertThat(storedFile.getStoreFilePath()).exists()
            assertThat(listStoredFiles(transaction)).containsExactly(storedFile)

            // Get file without transaction
            mockMvc.perform(
                get("/api/section/${section.uuid}/file/${storedFile.uuid}")
                    .with(httpBasic("sirius", "sirius"))
            ).andExpect(status().isBadRequest)

            // Get file with transaction
            val readStoredFile: StoredFileDetails = mockMvc.perform(
                get("/api/section/${section.uuid}/file/${storedFile.uuid}?transactionUuid=${transaction.uuid}")
                    .with(httpBasic("sirius", "sirius"))
            ).andExpect(status().isOk)
                .andReturn()
                .payload(objectMapper)
            assertThat(storedFile).isEqualTo(readStoredFile)

            // Commit transaction
            mockMvc.perform(
                post("/api/transaction/${transaction.uuid}/commit")
                    .with(httpBasic("sirius", "sirius"))
            ).andExpect(status().isOk)
            assertThat(transactionRepository.existsById(transaction.uuid)).isFalse

            mockMvc.perform(
                get("/api/section/${section.uuid}/file/${storedFile.uuid}")
                    .with(httpBasic("sirius", "sirius"))
            ).andExpect(status().isOk)

            assertThat(storedFile.getStoreFilePath()).exists()
        }

        @Test
        fun testRollback() {
            // Save file
            val storedFile: StoredFileDetails = mockMvc.perform(
                multipart("/api/section/${section.uuid}/file")
                    .file(partFile)
                    .text("transactionUuid", transaction.uuid.toString())
                    .with(httpBasic("sirius", "sirius"))
            ).andExpect(status().isOk)
                .andReturn()
                .payload(objectMapper)

            val storedPath = storedFile.getStoreFilePath()
            assertThat(storedPath).exists()

            // Get file with transaction
            val readStoredFile: StoredFileDetails = mockMvc.perform(
                get("/api/section/${section.uuid}/file/${storedFile.uuid}?transactionUuid=${transaction.uuid}")
                    .with(httpBasic("sirius", "sirius"))
            ).andExpect(status().isOk)
                .andReturn()
                .payload(objectMapper)
            assertThat(storedFile).isEqualTo(readStoredFile)

            // Rollback transaction
            mockMvc.perform(
                post("/api/transaction/${transaction.uuid}/rollback")
                    .with(httpBasic("sirius", "sirius"))
            ).andExpect(status().isOk)
            assertThat(transactionRepository.existsById(transaction.uuid)).isFalse

            mockMvc.perform(
                get("/api/section/${section.uuid}/file/${storedFile.uuid}")
                    .with(httpBasic("sirius", "sirius"))
            ).andExpect(status().isBadRequest)

            assertThat(storedPath).doesNotExist()
        }

        private fun listStoredFiles(transaction: StorageTransaction): List<StoredFileDetails> {
            return mockMvc.perform(
                get("/api/transaction/${transaction.uuid}/file")
                    .with(httpBasic("sirius", "sirius"))
            ).andExpect(status().isOk)
                .andReturn()
                .payload(objectMapper)
        }
    }

    @Nested
    inner class TransactionalSection {

        @Test
        fun testCommit() {
            val section = sectionRepository.findByCodename("committedSection")
                ?: createSection(SectionCreateRequest(codename = "committedSection", secure = true, transactionUuid = transaction.uuid))

            // Get section without transaction
            mockMvc.perform(
                get("/api/section/${section.uuid}")
                    .with(httpBasic("sirius", "sirius"))
            ).andExpect(status().isBadRequest)

            val readSection: Section = mockMvc.perform(
                get("/api/section/${section.uuid}?transactionUuid=${transaction.uuid}")
                    .with(httpBasic("sirius", "sirius"))
            ).andExpect(status().isOk)
                .andReturn()
                .payload(objectMapper)

            assertThat(readSection).isEqualTo(section)

            // Try to save file without transaction
            mockMvc.perform(
                multipart("/api/section/${section.uuid}/file")
                    .file(partFile)
                    .with(httpBasic("sirius", "sirius"))
            ).andExpect(status().isBadRequest)

            mockMvc.perform(
                multipart("/api/section/${section.uuid}/file")
                    .file(partFile)
                    .text("transactionUuid", transaction.uuid.toString())
                    .with(httpBasic("sirius", "sirius"))
            ).andExpect(status().isOk)

            // Commit transaction
            mockMvc.perform(
                post("/api/transaction/${transaction.uuid}/commit")
                    .with(httpBasic("sirius", "sirius"))
            ).andExpect(status().isOk)
            assertThat(transactionRepository.existsById(transaction.uuid)).isFalse

            mockMvc.perform(
                get("/api/section/${section.uuid}")
                    .with(httpBasic("sirius", "sirius"))
            ).andExpect(status().isOk)
        }

        @Test
        fun testRollback() {
            val section = sectionRepository.findByCodename("rollbackSection")
                ?: createSection(SectionCreateRequest(codename = "rollbackSection", secure = true, transactionUuid = transaction.uuid))

            // Save file
            mockMvc.perform(
                multipart("/api/section/${section.uuid}/file")
                    .file(partFile)
                    .text("transactionUuid", transaction.uuid.toString())
                    .with(httpBasic("sirius", "sirius"))
            ).andExpect(status().isOk)

            // Commit transaction
            mockMvc.perform(
                post("/api/transaction/${transaction.uuid}/rollback")
                    .with(httpBasic("sirius", "sirius"))
            ).andExpect(status().isOk)
            assertThat(transactionRepository.existsById(transaction.uuid)).isFalse

            mockMvc.perform(
                get("/api/section/${section.uuid}")
                    .with(httpBasic("sirius", "sirius"))
            ).andExpect(status().isBadRequest)
        }

        @Test
        fun testMixedSections() {
            val section = sectionRepository.findByCodename("ciphered")
                ?: createSection(SectionCreateRequest(codename = "ciphered", secure = true))

            val txSection = sectionRepository.findByCodename("committedSection")
                ?: createSection(SectionCreateRequest(codename = "committedSection", secure = true, transactionUuid = transaction.uuid))

            // Save file in non-transactional section
            val file1: StoredFileDetails = mockMvc.perform(
                multipart("/api/section/${section.uuid}/file")
                    .file(partFile)
                    .text("transactionUuid", transaction.uuid.toString())
                    .with(httpBasic("sirius", "sirius"))
            ).andExpect(status().isOk)
                .andReturn()
                .payload(objectMapper)

            // Save file in transactional section
            val file2: StoredFileDetails = mockMvc.perform(
                multipart("/api/section/${txSection.uuid}/file")
                    .file(partFile)
                    .text("transactionUuid", transaction.uuid.toString())
                    .with(httpBasic("sirius", "sirius"))
            ).andExpect(status().isOk)
                .andReturn()
                .payload(objectMapper)

            mockMvc.perform(
                post("/api/transaction/${transaction.uuid}/commit")
                    .with(httpBasic("sirius", "sirius"))
            ).andExpect(status().isOk)
            assertThat(transactionRepository.existsById(transaction.uuid)).isFalse
            assertThat(storedFileRepository.existsById(file1.uuid)).isTrue
            assertThat(storedFileRepository.existsById(file2.uuid)).isTrue
        }
    }

    @Nested
    inner class MultiTransaction {

        private lateinit var transaction2: StorageTransaction

        @BeforeEach
        fun before() {
            transaction2 = mockMvc.perform(
                post("/api/transaction")
                    .with(httpBasic("sirius", "sirius"))
                    .json(StorageTransaction())
            ).andExpect(status().isOk)
                .andReturn()
                .payload(objectMapper)
        }

        @Test
        fun testMultiSections() {
            val section1 = sectionRepository.findByCodename("ciphered")
                ?: createSection(SectionCreateRequest(codename = "section1", secure = true, transactionUuid = transaction.uuid))

            val section2 = sectionRepository.findByCodename("committedSection")
                ?: createSection(SectionCreateRequest(codename = "section2", secure = true, transactionUuid = transaction2.uuid))

            // Save file in transactional section
            val file1: StoredFileDetails = mockMvc.perform(
                multipart("/api/section/${section1.uuid}/file")
                    .file(partFile)
                    .text("transactionUuid", transaction.uuid.toString())
                    .with(httpBasic("sirius", "sirius"))
            ).andExpect(status().isOk)
                .andReturn()
                .payload(objectMapper)

            mockMvc.perform(
                get("/api/section/${section2.uuid}/file/${file1.uuid}?transactionUuid=${transaction.uuid}")
                    .with(httpBasic("sirius", "sirius"))
            ).andExpect(status().isBadRequest)
        }
    }
}