package io.sirius.storage.dto

import java.time.Instant
import java.util.*

data class SectionDetails(
    val uuid: UUID,
    val creationDate: Instant,
    val codename: String,
    val secured: Boolean,
    val metadata: Map<String, String>,
    val files: List<StoredFileDetails>,
    val totalFilesSize: Long = files.sumOf { it.fileSize },
)