package io.sirius.storage

import com.fasterxml.jackson.databind.ObjectMapper
import com.fasterxml.jackson.module.kotlin.readValue
import org.springframework.core.io.Resource
import org.springframework.mock.web.MockMultipartFile
import org.springframework.test.web.servlet.MvcResult
import org.springframework.test.web.servlet.request.MockMultipartHttpServletRequestBuilder
import org.springframework.test.web.servlet.request.RequestPostProcessor

object TestExtensions {

    fun Resource.toByteArray(): ByteArray = inputStream.use { it.readAllBytes() }

    inline fun <reified T> MvcResult.payload(mapper: ObjectMapper): T =
        mapper.readValue(this.response.contentAsString)

    fun httpMethod(method: String): RequestPostProcessor = RequestPostProcessor { request ->
        request.method = method
        request
    }

    fun MockMultipartHttpServletRequestBuilder.text(
        name: String,
        value: String
    ): MockMultipartHttpServletRequestBuilder = file(MockMultipartFile(name, "", "text/plain", value.toByteArray()))

}