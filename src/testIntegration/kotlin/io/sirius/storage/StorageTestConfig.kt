package io.sirius.storage

import com.google.common.jimfs.Jimfs
import org.springframework.context.annotation.Bean
import org.springframework.context.annotation.Configuration
import org.springframework.context.annotation.Primary
import java.nio.file.FileSystem
import javax.crypto.KeyGenerator
import javax.crypto.SecretKey

@Configuration
class StorageTestConfig {

    @Primary
    @Bean
    fun tempFileSystem(): FileSystem = Jimfs.newFileSystem()

    @Bean
    fun masterKey(): SecretKey {
        val keyGen = KeyGenerator.getInstance("AES")
        keyGen.init(128)
        return keyGen.generateKey()
    }
}