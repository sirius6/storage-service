package io.sirius.storage

import com.fasterxml.jackson.databind.ObjectMapper
import io.sirius.storage.TestExtensions.payload
import io.sirius.storage.config.StorageProperties
import io.sirius.storage.dto.SectionCreateRequest
import io.sirius.storage.dto.StoredFileDetails
import io.sirius.storage.model.Section
import io.sirius.storage.repository.SectionRepository
import io.sirius.storage.repository.StorageTransactionRepository
import io.sirius.storage.repository.StoredFileMetadataRepository
import io.sirius.storage.repository.StoredFileRepository
import io.sirius.storage.util.findByUuid
import org.junit.jupiter.api.extension.ExtendWith
import org.springframework.beans.factory.annotation.Autowired
import org.springframework.boot.test.autoconfigure.web.servlet.AutoConfigureMockMvc
import org.springframework.boot.test.context.SpringBootTest
import org.springframework.boot.web.server.LocalServerPort
import org.springframework.http.MediaType
import org.springframework.security.test.web.servlet.request.SecurityMockMvcRequestPostProcessors
import org.springframework.test.context.ActiveProfiles
import org.springframework.test.context.junit.jupiter.SpringExtension
import org.springframework.test.web.servlet.MockMvc
import org.springframework.test.web.servlet.request.MockHttpServletRequestBuilder
import org.springframework.test.web.servlet.request.MockMvcRequestBuilders
import org.springframework.test.web.servlet.result.MockMvcResultMatchers
import java.nio.file.FileSystem
import java.nio.file.Path

@SpringBootTest(webEnvironment = SpringBootTest.WebEnvironment.RANDOM_PORT)
@ExtendWith(SpringExtension::class)
@ActiveProfiles("dev", "test")
@AutoConfigureMockMvc
abstract class AbstractTest {

    @Autowired
    protected lateinit var mockMvc: MockMvc

    @LocalServerPort
    protected var serverPort: Int = 8080

    @Autowired
    protected lateinit var objectMapper: ObjectMapper

    @Autowired
    protected lateinit var fileSystem: FileSystem

    @Autowired
    protected lateinit var properties: StorageProperties

    @Autowired
    protected lateinit var sectionRepository: SectionRepository

    @Autowired
    protected lateinit var storedFileRepository: StoredFileRepository

    @Autowired
    protected lateinit var metadataRepository: StoredFileMetadataRepository

    @Autowired
    protected lateinit var transactionRepository: StorageTransactionRepository

    fun payload(obj: Any): String = objectMapper.writeValueAsString(obj)

    fun StoredFileDetails.getStoreFilePath(): Path {
        val section = storedFileRepository.findByUuid(uuid)!!.section

        return fileSystem.getPath(properties.directory)
            .resolve("sections").resolve(section.uuid.toString())
            .resolve("files").resolve(uuid.toString())
    }

    protected fun createSection(request: SectionCreateRequest): Section = mockMvc.perform(
        MockMvcRequestBuilders.post("/api/section")
            .json(request)
            .with(SecurityMockMvcRequestPostProcessors.httpBasic("sirius", "sirius"))
    ).andExpect(MockMvcResultMatchers.status().isOk)
        .andReturn()
        .payload(objectMapper)

    fun MockHttpServletRequestBuilder.json(obj: Any): MockHttpServletRequestBuilder =
        contentType(MediaType.APPLICATION_JSON).content(payload(obj))

}