package io.sirius.storage.batch

import io.sirius.storage.AbstractTest
import io.sirius.storage.TestExtensions.payload
import io.sirius.storage.TestExtensions.text
import io.sirius.storage.dto.StoredFileDetails
import io.sirius.storage.model.Section
import io.sirius.storage.model.StorageTransaction
import org.assertj.core.api.Assertions
import org.assertj.core.api.Assertions.assertThat
import org.junit.jupiter.api.Test
import org.springframework.beans.factory.annotation.Autowired
import org.springframework.mock.web.MockMultipartFile
import org.springframework.security.test.web.servlet.request.SecurityMockMvcRequestPostProcessors.httpBasic
import org.springframework.test.web.servlet.request.MockMvcRequestBuilders.multipart
import org.springframework.test.web.servlet.result.MockMvcResultMatchers.status
import java.time.Instant
import java.time.temporal.ChronoUnit
import java.util.*

class StorageTransactionExpirationTest : AbstractTest() {

    @Autowired
    private lateinit var batchRunner: BatchRunner

    private val now = Instant.now()

    @Test
    fun testExpiredTransaction() {
        val transaction = transactionRepository.save(
            StorageTransaction(expirationDate = now.minus(20, ChronoUnit.MINUTES))
        )

        val section1 = sectionRepository.save(Section(codename = "txExpired1"))
        val section2 = sectionRepository.save(Section(codename = "txExpired2"))

        val file1 = section1.storeFile(
            MockMultipartFile("file", "file1.txt", "text/plain", "hello".toByteArray()),
            transaction.uuid
        )
        val file2 = section2.storeFile(
            MockMultipartFile("file", "file2.txt", "text/plain", "hello".toByteArray()),
            transaction.uuid
        )
        val file3 = section2.storeFile(
            MockMultipartFile("file", "file3.txt", "text/plain", "hello".toByteArray()),
            transaction.uuid
        )

        batchRunner.executeExpirationJob()
        assertThat(transactionRepository.existsById(transaction.uuid)).isFalse
        assertThat(storedFileRepository.existsById(file1.uuid)).isFalse
        assertThat(storedFileRepository.existsById(file2.uuid)).isFalse
        assertThat(storedFileRepository.existsById(file3.uuid)).isFalse
    }

    @Test
    fun testTransactionNotYetExpired() {
        val transaction = transactionRepository.save(
            StorageTransaction(expirationDate = now.plus(20, ChronoUnit.MINUTES))
        )

        val section1 = sectionRepository.save(Section(codename = "txNotExpired1"))
        val section2 = sectionRepository.save(Section(codename = "txNotExpired2"))

        val file1 = section1.storeFile(
            MockMultipartFile("file", "file1.txt", "text/plain", "hello".toByteArray()),
            transaction.uuid
        )
        val file2 = section2.storeFile(
            MockMultipartFile("file", "file2.txt", "text/plain", "hello".toByteArray()),
            transaction.uuid
        )

        batchRunner.executeExpirationJob()
        assertThat(transactionRepository.existsById(transaction.uuid)).isTrue
        assertThat(storedFileRepository.existsById(file1.uuid)).isTrue
        assertThat(storedFileRepository.existsById(file2.uuid)).isTrue
    }

    private fun Section.storeFile(
        file: MockMultipartFile,
        transactionUuid: UUID
    ): StoredFileDetails = mockMvc.perform(
        multipart("/api/section/$uuid/file")
            .file(file)
            .text("transactionUuid", transactionUuid.toString())
            .with(httpBasic("sirius", "sirius"))
    ).andExpect(status().isOk)
        .andReturn()
        .payload(objectMapper)
}