package io.sirius.storage.batch

import io.sirius.storage.AbstractTest
import io.sirius.storage.TestExtensions.payload
import io.sirius.storage.TestExtensions.text
import io.sirius.storage.dto.StoredFileDetails
import io.sirius.storage.model.Section
import org.assertj.core.api.Assertions.assertThat
import org.junit.jupiter.api.BeforeEach
import org.junit.jupiter.api.Test
import org.springframework.beans.factory.annotation.Autowired
import org.springframework.mock.web.MockMultipartFile
import org.springframework.security.test.web.servlet.request.SecurityMockMvcRequestPostProcessors.httpBasic
import org.springframework.test.web.servlet.request.MockMvcRequestBuilders
import org.springframework.test.web.servlet.request.MockMvcRequestBuilders.multipart
import org.springframework.test.web.servlet.result.MockMvcResultMatchers
import org.springframework.test.web.servlet.result.MockMvcResultMatchers.status
import java.time.Instant

class StoredFileExpirationTest : AbstractTest() {

    @Autowired
    private lateinit var batchRunner: BatchRunner

    private val now = Instant.now()
    private lateinit var section: Section

    @BeforeEach
    fun before() {
        section = sectionRepository.findByCodename("storedFileExpiration")
            ?: sectionRepository.save(Section(codename = "storedFileExpiration"))
    }

    @Test
    fun testExpiredFile() {
        val file1 = section.storeFile(
            MockMultipartFile("file", "file1.txt", "text/plain", "hello".toByteArray()),
            now.minusSeconds(30)
        )

        batchRunner.executeExpirationJob()
        assertThat(storedFileRepository.existsById(file1.uuid)).isFalse
    }

    @Test
    fun testFileNotYetExpired() {
        val file1 = section.storeFile(
            MockMultipartFile("file", "file2.txt", "text/plain", "hello".toByteArray()),
            now.plusSeconds(30)
        )

        batchRunner.executeExpirationJob()
        assertThat(storedFileRepository.existsById(file1.uuid)).isTrue
    }

    @Test
    fun testFileNeverExpired() {
        val file1 = section.storeFile(
            MockMultipartFile("file", "file3.txt", "text/plain", "hello".toByteArray()),
            null
        )

        batchRunner.executeExpirationJob()
        assertThat(storedFileRepository.existsById(file1.uuid)).isTrue
    }

    private fun Section.storeFile(
        file: MockMultipartFile,
        expirationDate: Instant?
    ): StoredFileDetails = mockMvc.perform(
        multipart("/api/section/$uuid/file")
            .file(file)
            .also { builder -> expirationDate?.let { builder.text("expirationDate", it.toString()) } }
            .with(httpBasic("sirius", "sirius"))
    ).andExpect(status().isOk)
        .andReturn()
        .payload(objectMapper)
}