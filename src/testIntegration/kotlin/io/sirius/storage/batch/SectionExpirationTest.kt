package io.sirius.storage.batch

import io.sirius.storage.AbstractTest
import io.sirius.storage.TestExtensions.payload
import io.sirius.storage.dto.SectionCreateRequest
import io.sirius.storage.dto.StoredFileDetails
import io.sirius.storage.model.Section
import org.assertj.core.api.Assertions.assertThat
import org.junit.jupiter.api.Test
import org.springframework.beans.factory.annotation.Autowired
import org.springframework.mock.web.MockMultipartFile
import org.springframework.security.test.web.servlet.request.SecurityMockMvcRequestPostProcessors
import org.springframework.security.test.web.servlet.request.SecurityMockMvcRequestPostProcessors.httpBasic
import org.springframework.test.web.servlet.request.MockMvcRequestBuilders
import org.springframework.test.web.servlet.request.MockMvcRequestBuilders.multipart
import org.springframework.test.web.servlet.result.MockMvcResultMatchers
import org.springframework.test.web.servlet.result.MockMvcResultMatchers.status
import java.time.Instant

class SectionExpirationTest : AbstractTest() {

    @Autowired
    private lateinit var batchRunner: BatchRunner

    private val now = Instant.now()

    @Test
    fun testExpiredSection() {
        val section: Section = mockMvc.perform(
            MockMvcRequestBuilders.post("/api/section")
                .json(SectionCreateRequest(codename = "expired", secure = false, expirationDate = now.minusSeconds(30)))
                .with(httpBasic("sirius", "sirius"))
        ).andExpect(status().isOk)
            .andReturn()
            .payload(objectMapper)

        val file1 = section.storeFile(MockMultipartFile("file", "file1.txt", "text/plain", "hello".toByteArray()))
        val file2 = section.storeFile(MockMultipartFile("file", "file2.txt", "text/plain", "hello".toByteArray()))

        batchRunner.executeExpirationJob()

        assertThat(sectionRepository.existsById(section.uuid)).isFalse
        assertThat(storedFileRepository.existsById(file1.uuid)).isFalse
        assertThat(storedFileRepository.existsById(file2.uuid)).isFalse
    }

    @Test
    fun testSectionNotYetExpired() {
        val section: Section = mockMvc.perform(
            MockMvcRequestBuilders.post("/api/section")
                .json(SectionCreateRequest(codename = "notExpired", secure = false, expirationDate = now.plusSeconds(30)))
                .with(httpBasic("sirius", "sirius"))
        ).andExpect(status().isOk)
            .andReturn()
            .payload(objectMapper)

        val file1 = section.storeFile(MockMultipartFile("file", "file1.txt", "text/plain", "hello".toByteArray()))
        val file2 = section.storeFile(MockMultipartFile("file", "file2.txt", "text/plain", "hello".toByteArray()))

        batchRunner.executeExpirationJob()

        assertThat(sectionRepository.existsById(section.uuid)).isTrue
        assertThat(storedFileRepository.existsById(file1.uuid)).isTrue
        assertThat(storedFileRepository.existsById(file2.uuid)).isTrue
    }

    @Test
    fun testSectionWithoutExpiration() {
        val section: Section = mockMvc.perform(
            MockMvcRequestBuilders.post("/api/section")
                .json(SectionCreateRequest(codename = "noExpiration", secure = false, expirationDate = null))
                .with(httpBasic("sirius", "sirius"))
        ).andExpect(status().isOk)
            .andReturn()
            .payload(objectMapper)

        val file1 = section.storeFile(MockMultipartFile("file", "file1.txt", "text/plain", "hello".toByteArray()))
        val file2 = section.storeFile(MockMultipartFile("file", "file2.txt", "text/plain", "hello".toByteArray()))

        batchRunner.executeExpirationJob()

        assertThat(sectionRepository.existsById(section.uuid)).isTrue
        assertThat(storedFileRepository.existsById(file1.uuid)).isTrue
        assertThat(storedFileRepository.existsById(file2.uuid)).isTrue
    }

    private fun Section.storeFile(file: MockMultipartFile): StoredFileDetails = mockMvc.perform(
        multipart("/api/section/$uuid/file")
            .file(file)
            .with(httpBasic("sirius", "sirius"))
    ).andExpect(status().isOk)
        .andReturn()
        .payload(objectMapper)
}